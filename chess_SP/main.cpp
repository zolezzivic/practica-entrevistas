#include <iostream>
#include "tests.h"
#include "shortestPath.h"

int main() {
    Node node1 = Node(tuple<int,int> (3,2));
    Node node2 = Node(tuple<int,int> (0,5));
    int res = shortestPath(10,10, node1, node2);
    std::cout << "Size of shortest path: "<<res << std::endl;
    if (!runTests()) return 1;
    cout<<"All tests have been passed successfully"<<endl;
    return 0;
}
