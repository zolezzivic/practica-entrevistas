#ifndef CHESS_SP_SHORTESTPATH_H
#define CHESS_SP_SHORTESTPATH_H

#include <iostream>
#include <list>
#include <tuple>
#include <vector>
#include <queue>

using namespace std;

struct Node{
    tuple<int,int> pos;
    int x;
    int y;
    Node(tuple<int,int> position) : pos(position), x(get<0>(position)), y(get<1>(position)){};
    bool operator ==(const Node &other)const{
        return (x==other.x && y==other.y);
    }

};
ostream& operator << (ostream& os, Node n);

class ChessGraph{
    public:
        ChessGraph(int rows, int cols);
        list<Node> getAdjList(Node);
        vector<vector<int>> BFS(Node node0, Node node1);
    private:
        int _size_V;     //Number of vertices
        int _rows;
        int _cols;
        int _size_X;     //Number of edges
        vector<vector<list<Node>>> _adj; //_adj[i][j] stores the adjacency list of the (i,j) node, i.e. the squares
                                                //that can be reached by (i,j) by a knight move.

};
list<Node> possibleNeighbors(int row, int col, int rows, int cols);
int shortestPath(int rows, int cols, Node node1, Node node2);
void printDistances(vector<vector<int>>& d);
#endif //CHESS_SP_SHORTESTPATH_H
