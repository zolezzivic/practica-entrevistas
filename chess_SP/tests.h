#ifndef CHESS_SP_TESTS_H
#define CHESS_SP_TESTS_H

#include "shortestPath.h"

bool distanceEqualsOne();
bool knownSP(int rows, int cols, Node n1, Node n2, int res);
bool symmetricalDistance();
bool runTests();

#endif //CHESS_SP_TESTS_H
