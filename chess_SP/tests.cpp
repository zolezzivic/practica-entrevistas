#include "tests.h"


//This test checks that the algorithm returns 1 when we are going from one square to another when there is a knight move that connects them.
bool distanceEqualsOne(){
    for (int sizeBoard = 5; sizeBoard < 20; ++sizeBoard) {
        for (int i = 0; i < sizeBoard; ++i) {
            for (int j = 0; j < sizeBoard; ++j) {
                list<Node> neighbors = possibleNeighbors(i, j, sizeBoard, sizeBoard);
                for (Node n: neighbors) {
                    if (shortestPath(sizeBoard, sizeBoard, Node(tuple<int,int>(i,j)), n)!=1){
                        cout<<"The distanceEqualsOne test has failed"<<endl;
                        return false;
                    }
                }
            }
        }
    }
    return true;
}

//This tests checks that the results are correct when testing known distances. It also checks that the distances are preserved when moving to a
//bigger chess board.
bool knownSP(int rows, int cols, Node n1, Node n2, int res){
    for (int r = rows; r < 20; ++r) {
        for (int c = cols; c < 20; ++c) {
            if (shortestPath(r, c, n1, n2)!= res){
                cout<<"The distance between " << n1<<"and"<< n2<< " is not correct"<<endl;
                return false;
            }
        }
    }
    return true;
}

//This test checks that the distance from node1 to node2 equals the distance from nod2 to node1.
bool symmetricalDistance(){
    for (int r = 5; r < 20; ++r) {
        for (int c = 5; c < 20; ++c) {
            Node n1 = Node(tuple<int,int> (rand() % (r-1), rand() % (c-1)));
            Node n2 = Node(tuple<int,int> (rand() % (r-1), rand() % (c-1)));
            if (shortestPath(r,c, n1,n2)!= shortestPath(r,c, n2,n1)){
                cout<< "The symmetrical distance test has failed"<<endl;
                return false;
            }
        }
    }
    return true;
}


bool runTests(){
    if (!distanceEqualsOne()) return false;
    if (!symmetricalDistance()) return false;
    if (!knownSP(5,6, Node(tuple<int,int> (1,2)), Node(tuple<int,int> (4,5)), 2)) return false;
    if (!knownSP(5,6, Node(tuple<int,int> (4,0)), Node(tuple<int,int> (0,5)), 3)) return false;
    return true;


}