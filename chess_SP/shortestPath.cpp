#include "shortestPath.h"

//To solve this problem I will use a shortest path algorithm.
//Our graph will have the chess squares as nodes and we will create an edge connecting two nodes if
//they can reach the other one with a knigth move. This procedure has a complexity equivalent to O(number of squares).
//Notice that the distance between two nodes is equivalent to the amount of knight moves we must make to connect them.
//Then, we are interested in finding the shortest path between the nodes we receive as input. To do this, I will use
//the BFS algorithm, since our edges are not weighted. This algorithm is O(m), where m represents the amount of edges we
//have. Knowing that every position can reach at most 8 squares, m is O(number of squares).
//In conclusion, our algorithm is O(number of squares).


list<Node> possibleNeighbors(int row, int col, int rows, int cols) {
    list<Node> res;
    if (row+1<rows && col+2<cols){
        res.push_back(Node(tuple<int,int> (row+1, col+2)));
    }
    if (row+1<rows && col-2>= 0){
        res.push_back(Node(tuple<int,int> (row+1, col-2)));
    }
    if (row+2<rows && col+1<cols){
        res.push_back(Node(tuple<int,int> (row+2, col+1)));
    }
    if (row+2<rows && col-1>= 0){
        res.push_back(Node(tuple<int,int> (row+2, col-1)));
    }
    if (row-2>=0 && col+1<cols){
        res.push_back(Node(tuple<int,int> (row-2, col+1)));
    }
    if (row-2>=0 && col-1>=0){
        res.push_back(Node(tuple<int,int> (row-2, col-1)));
    }
    if (row-1>=0 && col+2<cols){
        res.push_back(Node(tuple<int,int> (row-1, col+2)));
    }
    if (row-1>=0 && col-2>=0) {
        res.push_back(Node(tuple<int,int>(row - 1, col - 2)));
    }
    return res;
}


ChessGraph::ChessGraph(int rows, int cols){
    _size_V=rows;
    _rows=rows;
    _cols = cols;
    list<Node> empty_list;
    _size_X=0;
    _adj = vector<vector<list<Node>>> (rows, vector<list<Node>> (cols, empty_list));
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            _adj[i][j]= possibleNeighbors(i, j, rows, cols);
            _size_X += _adj[i][j].size();
        }
    }

}

list<Node> ChessGraph::getAdjList(Node node){
    return _adj[node.x][node.y];
}

vector<vector<int>> ChessGraph::BFS(Node node0, Node node1) {
    vector<vector<int>> res (_rows, vector<int >(_cols, INT32_MAX));
    res[node0.x][node0.y] = 0;
    queue <Node> q;
    q.push(node0);
    while (!q.empty()){
        Node n = q.front();
        q.pop();
        for(Node adjNode : getAdjList(n)){
            if (res[adjNode.x][adjNode.y] == INT32_MAX){
                res[adjNode.x][adjNode.y] = res[n.x][n.y] + 1;
                if (adjNode==node1) return res;
                q.push(adjNode);
            }
        }
    }
    return res;
}

int shortestPath(int rows, int cols, Node node1, Node node2){
    ChessGraph graph =ChessGraph(rows, cols);
    vector<vector<int>> distances = graph.BFS(node1, node2);
    //printDistances(distances);
    return distances[node2.x][node2.y];
}


void printDistances(vector<vector<int>>& d){
    for (int i = 0; i < d.size(); ++i) {
        for (int j = 0; j < d[0].size(); ++j) {
            cout<<d[i][j]<<"\t";
        }
        cout<< endl;
    }
    cout<<endl;
}

ostream& operator << (ostream& os, const Node n){
    os<<"["<<n.x<<","<<n.y<<"]";
    return os;
}
