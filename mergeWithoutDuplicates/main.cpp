#include <iostream>
#include <vector>
#include <unordered_set>
#include <string>
#include <algorithm>
#include <queue>
using namespace std;

struct elemAndIdx{
    int val;
    int idx;
    elemAndIdx() : val(-1), idx(-1){};
    elemAndIdx(int valor, int indice) : val(valor), idx(indice){}
    bool operator<(const struct elemAndIdx& other) const{
        return val < other.val;
    }
};

elemAndIdx indexOfMin(priority_queue<elemAndIdx>& q){
    elemAndIdx arrayMin= q.top();
    q.pop();
    return arrayMin;
}


vector<int> mergeWithoutDuplicates(vector<vector<int>>&nums) {
    vector<int> indices(nums.size(), 0);
    vector<int> result;
    priority_queue<elemAndIdx> numbersPointedByIndices;
    for (int i = 0; i < nums.size(); ++i) {
        if(!nums[i].empty()) numbersPointedByIndices.push(elemAndIdx(nums[i][0]*-1, i));
    }

    elemAndIdx arrayOfMinNumber;
    int currentArray;
    int currentNumber;

    while(!numbersPointedByIndices.empty()){
        arrayOfMinNumber=indexOfMin(numbersPointedByIndices);
        currentArray=arrayOfMinNumber.idx;
        currentNumber=arrayOfMinNumber.val*-1;
        if(result.empty()||currentNumber>result.back()){
            result.push_back(currentNumber);
        }
        indices[currentArray]++;
        if(indices[currentArray]<nums[currentArray].size()) numbersPointedByIndices.push(elemAndIdx(nums[currentArray][indices[currentArray]]*-1, currentArray));
    }
    return result;
}

bool test(){
    int numberOfArrays=rand()%100;
    vector<vector<int>> nums(numberOfArrays);
    unordered_set<int> numbers;

    for (int i = 0; i < numberOfArrays; ++i) {
        vector<int> array(rand()%50);
        for (int j = 0; j < array.size(); ++j) {
            int newNumber=rand()%50;
            array[j]=newNumber;
            numbers.insert(newNumber);
        }
        sort(array.begin(), array.end());
        nums[i]=array;
    }

    vector<int> res=mergeWithoutDuplicates(nums);
    unordered_set<int> elementsOfRes;
    for (int k = 0; k < res.size()-1; ++k) {
        elementsOfRes.insert(res[k]);
        if(res[k]>=res[k+1]) return false;
    }
    elementsOfRes.insert(res[res.size()-1]);
    return elementsOfRes.size()==numbers.size();
}

int main() {
    printf("%i\n",test());
    return 0;
}
