#include <iostream>
#include <vector>
#include <queue>
#include <unordered_set>

using namespace std;

struct nodeDistanceAndColor{
    int node;
    int distance;
    char color;
    nodeDistanceAndColor(int n, int d, char c): node(n), distance(d), color(c){};
    nodeDistanceAndColor(): node(-1), distance(-1), color('n'){};
};


vector<int> shortestAlternatingPaths(int n, vector<vector<int>>& redEdges, vector<vector<int>>& blueEdges) {
    vector<int> distances(n, -1);
    queue<nodeDistanceAndColor> nextNodes;
    nextNodes.push(nodeDistanceAndColor(0, 0,'n'));
    nodeDistanceAndColor currentNode;
    unordered_set<int> visitedNodesThroughRed;
    unordered_set<int> visitedNodesThroughBlue;
    visitedNodesThroughBlue.insert(0);
    visitedNodesThroughRed.insert(0);

    while(!nextNodes.empty()){
        currentNode=nextNodes.front();
        if (distances[currentNode.node]==-1) distances[currentNode.node]=currentNode.distance;
        else distances[currentNode.node]=min(distances[currentNode.node], currentNode.distance);

        if(currentNode.color!='r'){
            for(int node=0; node<redEdges.size();node++){
                if(redEdges[node][0]==currentNode.node){
                    if(!visitedNodesThroughRed.count(redEdges[node][1])){
                        nextNodes.push(nodeDistanceAndColor(redEdges[node][1], currentNode.distance + 1, 'r'));
                        visitedNodesThroughRed.insert(redEdges[node][1]);
                    }
                }
            }
        }
        if(currentNode.color!='b'){
            for(int node=0; node<blueEdges.size();node++){
                if(blueEdges[node][0]==currentNode.node){
                    if(! visitedNodesThroughBlue.count(blueEdges[node][1])){
                        nextNodes.push(nodeDistanceAndColor(blueEdges[node][1], currentNode.distance + 1, 'b'));
                        visitedNodesThroughBlue.insert(blueEdges[node][1]);
                    }
                }
            }
        }
        nextNodes.pop();
    }
    return distances;
}



int main() {
    vector<vector<int>> red= {{0,1},{1,2},{2,3},{3,4}};
    vector<vector<int>> blue= {{1,2},{2,3},{3,1}};
    vector<int> res= shortestAlternatingPaths(5, red, blue);
    return 0;
}
