#include <iostream>
#include <vector>
#include <algorithm>

class Solution{
public:
    int minAbsDifferenceOfServerLoads(std::vector<int>& loads);
    int minAbsDifferenceOfServerLoadsPD(std::vector<int>& loads, int idx, int S1, const int sumLoads);

private:
    std::vector<std::vector<int>> _memo;
};


int main() {
    std::vector<int> s={4,4,4,4,12};
    Solution sol=Solution();
    std::cout << sol.minAbsDifferenceOfServerLoads(s) << std::endl;
    return 0;
}


//Complejidad: O(n*sumLoads)
int Solution::minAbsDifferenceOfServerLoads(std::vector<int> &loads) {
    int sumLoads=0;
    for(int i:loads) sumLoads+=i;
    _memo=std::vector<std::vector<int>>(sumLoads, std::vector<int>(loads.size(), -1));
    return minAbsDifferenceOfServerLoadsPD(loads, 0, 0, sumLoads);
}


int Solution::minAbsDifferenceOfServerLoadsPD (std::vector<int>& loads, int idx, int S1, const int sumLoads){
    int S2=(sumLoads-S1);
    if(idx==loads.size()) {
        return abs(S1-S2);
    }
    if(_memo[S1][idx]==-1){
        S1+=loads[idx];
        int onFirstServer= minAbsDifferenceOfServerLoadsPD(loads, idx + 1, S1, sumLoads);
        S1-=loads[idx];
        int onSecondServer= minAbsDifferenceOfServerLoadsPD(loads, idx + 1, S1, sumLoads);
        _memo[S1][idx]= std::min(onFirstServer, onSecondServer);
    }
    return _memo[S1][idx];
}