#include <iostream>
#include<vector>
#include <algorithm>

int binary_search(std::vector<int>& v, int house);
std::vector<int> closestStores(std::vector<int>& houses, std::vector<int>& stores);


int main() {
    std::vector<int> stores={5, 3, 1, 2, 6};
    std::vector<int> houses={4, 8, 1, 1};
    std::vector<int> res=closestStores(houses,stores);
    for (int i :res) {
        std::cout<<i<<std::endl;
    }
    return 0;
}


int binary_search(std::vector<int>& v, int house){
    int res=INT32_MAX;
    int l=0;
    int r=v.size();
    int mid;
    while(r-l>1){
        mid=(r+l)/2;
        if(v[mid]==house) return v[mid];
        if(abs(v[mid]-house)<abs(house-res)) res=v[mid];
        if(v[mid]<house){
            l=mid;
        }else{
            r=mid;
        }
    }
    if(abs(v[l]-house)<=abs(house-res)) res=v[l];
    if(abs(v[r-1]-house)<abs(house-res)) res=v[r-1];
    return res;
}

std::vector<int> closestStores(std::vector<int>& houses, std::vector<int>& stores){
    std::sort(stores.begin(), stores.end());
    std::vector<int> res(houses.size());
    for (int i = 0; i < houses.size(); ++i) {
        res[i]=binary_search(stores, houses[i]);
    }
    return res;
}
