#include <iostream>
#include <vector>
#include <unordered_set>

int amountPrefixPerm(std::vector<int>& A, int N);
int extremeNumber(std::vector<int>& A);
int maxRiceGrains(std::vector<std::vector<int>>& board);
int maxRiceGrainsBF(int row, int col,std::vector<std::vector<int>>& board);
bool testRice();
bool testPrefix();

int main() {
    std::vector<std::vector<int>> v={{2,2,4,2},
                                     {0,3,0,1},
                                     {1,2,2,1},
                                     {4,1,2,2}};
    std::cout << testPrefix() << std::endl;
    return 0;
}


int amountPrefixPerm(std::vector<int>& A, int N){
    int max=INT32_MIN;
    int amountLessThanMax=0;
    int res=0;
    for (int i = 0; i < N; ++i) {
        if(A[i]>N) return res;    //If there are numbers>N, I break because there wont be any other valid permutation
        if(A[i]>max) max=A[i];
        amountLessThanMax++;
        if(max==i+1 && amountLessThanMax==i+1) res++;
    }
    return res;
}

bool testPrefix(){
    std::unordered_set<int> arrayElements;
    for (int i = 1; i < 200; ++i) {
        arrayElements.clear();
        std::vector<int> A(i);
        for (int j = 0; j < i; ++j) {
            int newNumber=rand()%i+1;
            while(arrayElements.count(newNumber)) newNumber=rand()%i+1;
            arrayElements.insert(newNumber);
            A[j]=newNumber;
        }
        int numberOfPrefixIndices=0;
        for (int k = 0; k < i; ++k) {
            if(A[k]>k+1) continue;
            int numbersToK=0;
            for (int j = 0; j <= k; ++j) {
                if(A[j]<=k+1)numbersToK++;
            }
            if(numbersToK==k+1) numberOfPrefixIndices++;
        }
        if(numberOfPrefixIndices!=amountPrefixPerm(A, i)) {
            return false;
        }
    }
    return true;
}


//To handle the overflow, Ill divide each element of the array by n, since the element with max deviation will remain the same
//I could've used long.
int extremeNumber(std::vector<int>&A){
    float mean=0;
    float n=A.size();
    for(int i:A) mean+=((float)i/n)/n;
    float max=INT32_MIN;
    int idxMax=-1;

    for (int j = 0; j < n; ++j) {
        float curr=A[j]/n;
        float deviation=std::abs(curr-mean);
        if(deviation>max){
            max=deviation;
            idxMax=j;
        }
    }
    return idxMax;
}

//Complexity: O(NM)time, O(2M)space
int maxRiceGrains(std::vector<std::vector<int>>& board){
    int N=board.size();
    int M=board[0].size();
    if(N==1&&M==1) return board[0][0];
    std::vector<int> prevRow(M);
    std::vector<int> currRow(M);
    prevRow[0]=board[0][0];
    for (int col = 1; col < M; ++col){
        prevRow[col]=prevRow[col-1]+board[0][col];  //First row positions can be reach by one square (the previous one)
    }
    if(N==1) return prevRow[M-1];


    for (int row = 1; row < N; ++row) {
        currRow[0]=prevRow[0]+board[row][0];
        for (int col = 1; col < M; ++col) {
            currRow[col]=std::max(currRow[col-1], prevRow[col]);
            currRow[col]+=board[row][col];
        }
        for(int i=0; i<M; i++) prevRow[i]=currRow[i];
    }
    return currRow[M-1];
}

int maxRiceGrainsBF(int row, int col,std::vector<std::vector<int>>& board){
    int N=board.size();
    int M=board[0].size();
    if(row==0&&col==0) return board[0][0];

    if(row==0) return board[row][col]+maxRiceGrainsBF(row, col-1, board);
    else if(col==0) return board[row][col]+maxRiceGrainsBF(row-1, col, board);
    else return board[row][col]+std::max(maxRiceGrainsBF(row, col-1, board), maxRiceGrainsBF(row-1, col, board));
}

bool testRice(){
    int N,M;
    for (int i = 1; i < 15; ++i) {
        N=i;
        for (int j = 1; j < 15; ++j) {
            M=j;
            std::vector<std::vector<int>> board(N, std::vector<int>(M));
            for (int k = 0; k < N; ++k) {
                for (int l = 0; l < M; ++l) {
                    board[k][l]=rand()%201;
                }
            }
            int rice=maxRiceGrains(board);
            int riceBF=maxRiceGrainsBF(N-1,M-1, board);
            if(rice!=riceBF){
                return false;
            }
        }
    }
    return true;
}