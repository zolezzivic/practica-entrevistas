#include <iostream>

std::string licenseKeyFormatting(std::string s, int k);

int main() {
    std::string s={"---"};
    std::cout << licenseKeyFormatting(s, 3) << std::endl;
    return 0;
}

std::string licenseKeyFormatting(std::string s, int k){
    std::string res;
    int numberOfDashes=0;
    for(char c:s){
        if(c=='-') numberOfDashes++;
    }
    int numberOfChars=s.size()-numberOfDashes;
    int numberOfGroups=numberOfChars/k;
    int numberOfCharsFirstGroup=numberOfChars%k;

    int currentChar=-1;
    int n=0;
    while(n<numberOfCharsFirstGroup){
        currentChar++;
        if(s[currentChar]!='-'){
            if(isdigit(s[currentChar])) res.push_back(s[currentChar]);
            else if(isupper(s[currentChar])){
                res.push_back(toupper(s[currentChar]));
            }
            else res.push_back(s[currentChar]);
            n++;
        }

    }
    if (currentChar!=-1 && currentChar<s.size())res.push_back('-');
    for (int i = 0; i < numberOfGroups; ++i) {
        int j=0;
        while(j<k){
            currentChar++;
            if(s[currentChar]!='-') {
                if(isdigit(s[currentChar])) res.push_back(s[currentChar]);
                else if(islower(s[currentChar])){
                    res.push_back(toupper(s[currentChar]));
                }else res.push_back(s[currentChar]);
                j++;
            }
        }
        if(currentChar<s.size()-1){
            res.push_back('-');
        }
    }
    if(!res.empty()&& res[res.size()-1]=='-'){
        res.pop_back();
    }
    return res;
}