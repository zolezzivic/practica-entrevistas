#include <iostream>
#include <unordered_map>


int keyBoardTime(std::string& keyboard, std::string& word);



int main() {
    std::string keyboard="abcdefghijklmnopqrstuvwxy";
    std::string text="cba";
    std::cout << keyBoardTime(keyboard, text) << std::endl;
    return 0;
}


int keyBoardTime(std::string& keyboard, std::string& word){
    std::unordered_map<char,int> charPositions;
    for (int i = 0; i < 26; ++i) {      //keyboard's lenght is always 26
        charPositions.insert(std::make_pair(keyboard[i],i));
    }
    int res=0;
    int lastPosition=0;
    for (int j = 0; j < word.size(); ++j) {
        res+= abs(lastPosition-charPositions[word[j]]);
        lastPosition=charPositions[word[j]];
    }
    return res;
}