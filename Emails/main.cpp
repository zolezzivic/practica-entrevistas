#include <iostream>
#include <vector>
#include <unordered_set>

int numUniqueEmails(std::vector<std::string>& emails);

int main() {
    std::vector<std::string> mails={"test.email+alex@leetcode.com", "test.email@leetcode.com"};
    std::cout << numUniqueEmails(mails)<< std::endl;
    return 0;
}

int numUniqueEmails(std::vector<std::string>& emails){
    std::unordered_set<std::string> mails;
    std::string actualEmail;
    bool ignoreUntilAt=false;
    bool passedAt=false;
    for(std::string mail: emails){
        actualEmail="";
        ignoreUntilAt=false;
        passedAt=false;
        for(char c:mail){
            switch(c){
                case '.':
                    if(passedAt) actualEmail.push_back(c);
                    break;
                case '+':
                    ignoreUntilAt=true;
                    break;
                case '@':
                    passedAt=true;
                    ignoreUntilAt=false;
                    actualEmail.push_back(c);
                    break;
                default:
                    if(!ignoreUntilAt) actualEmail.push_back(c);


            }
        }
        mails.insert(actualEmail);
    }
    return mails.size();
}