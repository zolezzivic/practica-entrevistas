#ifndef KNIGHTS_TOUR_TEST_H
#define KNIGHTS_TOUR_TEST_H
#include "knightsTour.h"
bool test_kt(int rows, int cols, int row_0, int col_0);
bool test_kt_aux(int rows, int cols, int row_0, int col_0, vector<tuple<int, int>> &moves);
#endif //KNIGHTS_TOUR_TEST_H
