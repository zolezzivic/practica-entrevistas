#include "knightsTour.h"

//To solve this problem I will use a Backtracking algorithm.
//The idea will be to analyse every possible sequence of knight moves until finding the one that corresponds
// to a Knight's Tour, i.e, it is a closed cycle.
//
//The algorithm will choose valid positions of the chessboard, i.e. squares that are reachable from the previous
// one by performing a knight move, until there are not possible new positions left. In that case, we will
// check if we have chosen every square and if the circuit is closed, because that means we found our Knight's Tour.
// If we haven't, we have to go backwards.
//Going backward in our algorithm means choosing a different square for the last position we have chosen. If that's
// not possible, we will have to discard the last two squares and choose a new one. This process will be repeated
// until there is a position that can be changed by doing a valid move.
//It is important to recall that we will move to a new square only if we can reach it with a knight move from our
// current position, and if it has not been chosen yet.
//
//To check if a square has been chosen, we will use a global matrix (positions) of 64 positions initialized with
// zeros. When a square (i,j) is chosen by the algorithm, will set positions[i][j]=1.
//
//
//
//Knowing that, in the worst case, every position can be reached by 8 different squares, and we have to determine
// 64 movements, then we will have a complexity equivalent to O(8^64). It is clear that an algorithm with this
// complexity is almost impossible to run, because it takes too long. That's why we make sure we only analyse sequences
//that can be extended to a valid solution, i.e., if when we move we run out of squares that can reach the starting square,
//we know that this partial solution won't become a valid one, since it will not be possible to close the tour.


bool found=false;
vector<vector<int>> positions;
vector<tuple<int,int>> squares_that_reach_first;
vector<tuple<int, int>> moves={};
int row_0, col_0;




//This function checks if moving to the (row,col) square implies running out of positions that can reach the starting square
bool reachable_squares(int visited_squares, int rows, int cols) {
    bool first_is_reachable=false;
    if (visited_squares >= rows*cols-2){        //If we visited all squares, we just need to close the cycle
        return true;
    }
    for (int i = 0; i < squares_that_reach_first.size(); ++i) {
        if (positions[get<0>(squares_that_reach_first[i])][get<1>(squares_that_reach_first[i])]==0){
            first_is_reachable=true;
            break;
        }
    }
    return first_is_reachable;
}

int number_of_neighbors(int row, int col, int rows, int cols) {
    int res=0;
    if (row+1<rows && col+2<cols){
        if (positions[row+1][col+2]!=1){
            res++;
        }
    }
    if (row+1<rows && col-2>= 0){
        if (positions[row+1][col-2]!=1){
            res++;
        }
    }
    if (row+2<rows && col+1<cols){
        if (positions[row+2][col+1]!=1){
            res++;
        }
    }
    if (row+2<rows && col-1>= 0){
        if (positions[row+2][col-1]!=1){
            res++;
        }
    }
    if (row-2>=0 && col+1<cols){
        if (positions[row-2][col+1]!=1){
            res++;
        }
    }
    if (row-2>=0 && col-1>=0){
        if (positions[row-2][col-1]!=1){
            res++;
        }
    }
    if (row-1>=0 && col+2<cols){
        if (positions[row-1][col+2]!=1){
            res++;
        }
    }
    if (row-1>=0 && col-2>=0) {
        if (positions[row - 1][col - 2] != 1) {
            res++;
        }
    }
    return res;
}

vector<tuple<int, int>> possible_neighbors(int row, int col, int rows, int cols) {
    vector<tuple<int,int>> res;
    if (row+1<rows && col+2<cols){
        if (positions[row+1][col+2]!=1){
            res.push_back(tuple<int,int> (row+1, col+2));

        }
    }
    if (row+1<rows && col-2>= 0){
        if (positions[row+1][col-2]!=1){
            res.push_back(tuple<int,int> (row+1, col-2));

        }
    }
    if (row+2<rows && col+1<cols){
        if (positions[row+2][col+1]!=1){
            res.push_back(tuple<int,int> (row+2, col+1));
        }
    }
    if (row+2<rows && col-1>= 0){
        if (positions[row+2][col-1]!=1){
            res.push_back(tuple<int,int> (row+2, col-1));

        }
    }
    if (row-2>=0 && col+1<cols){
        if (positions[row-2][col+1]!=1){
            res.push_back(tuple<int,int> (row-2, col+1));

        }
    }
    if (row-2>=0 && col-1>=0){
        if (positions[row-2][col-1]!=1){
            res.push_back(tuple<int,int> (row-2, col-1));
        }
    }
    if (row-1>=0 && col+2<cols){
        if (positions[row-1][col+2]!=1){
            res.push_back(tuple<int,int> (row-1, col+2));
        }
    }
    if (row-1>=0 && col-2>=0) {
        if (positions[row - 1][col - 2] != 1) {
            res.push_back(tuple<int, int>(row - 1, col - 2));
        }
    }
    return res;
}

//This function will sort the neighbors vector based on how many neighbors has each neighbor in the vector in
// ascending order.
void sort_by_neighbors(vector<tuple<int, int>> &neighbors, int rows, int cols) {
    vector<neigs_neig> aux;
    for (tuple<int,int> t: neighbors){
        //aux.push_back(neigs_neig(t, possible_neighbors(get<0>(t), get<1>(t), rows, cols).size()));

        aux.push_back(neigs_neig(t, number_of_neighbors(get<0>(t), get<1>(t), rows, cols)));
    }
    sort(aux.begin(), aux.end());
    for (int i = 0; i < neighbors.size(); ++i) {
        neighbors[i]=aux[i].neighbor;
    }
}


void KT_aux(int row, int col, int rows, int cols, vector<tuple<int, int>> &res) {
    if (found) return;
    if(res.size()>=rows*cols){ //If we have chosen every square
        if (row==row_0 && col==col_0){   //If our current position is starting one
            moves=res;                                                          //we store our current solution
            found=true;
        }
        return;
    }
    if (row==row_0 && col==col_0 && !res.empty()) return;    //We can't choose the starting square if we haven't visited all squares yet
    int new_row, new_col;
    vector<tuple<int,int>> neighbors = possible_neighbors(row, col, rows, cols);
    sort_by_neighbors(neighbors, rows, cols);
    for (tuple<int,int> t: neighbors) {
        if (found) return;
        new_row=get<0>(t);
        new_col=get<1>(t);
        positions[new_row][new_col]=1;
        if(reachable_squares(res.size(), rows, cols)) {
            res.push_back(t);
            KT_aux(new_row, new_col, rows, cols, res);
            res.pop_back();
        }
        positions[new_row][new_col]=0;

    }
}

vector<tuple<int, int>> KT(int rows, int cols, int r_0, int c_0) {
    moves={};
    found=false;
    row_0=r_0;
    col_0=c_0;
    vector<tuple<int,int>> res={};
    positions=vector<vector<int>> (rows, vector<int>(cols,0));
    squares_that_reach_first= possible_neighbors(row_0, col_0, rows, cols);
    KT_aux(r_0, c_0, rows, cols, res);
    return moves;
}

void print_vector(vector<tuple<int,int>>& vec){
    cout<<"[";
    for (int i = 0; i < vec.size(); ++i) {
        if (i!=vec.size()-1){
            cout<<"("<<get<0>(vec[i])<<","<< get<1>(vec[i])<<"),";
        }
        cout<<"("<<get<0>(vec[i])<<","<< get<1>(vec[i])<<")";
    }
    cout<<"]"<<endl;
}

