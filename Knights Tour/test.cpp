#include "test.h"

//To test if the algorithm is correct, I will check that every position is reachable by the previous one by performing
// a knight move. Apart from that, we must ensure that no square is visited more than once and that the Tour is closed, i.e. we need to check if the starting
// position is the last one and that all squares were visited. If those conditions are true, then the algorithm
// is correct.

bool test_kt_aux(int rows, int cols, int row_0, int col_0, vector<tuple<int, int>> &moves) {
    int t1_row, t1_col, t2_row, t2_col;
    if (moves.size()!= rows*cols){
        return false;
    }
    //Check if the tour starts and ends in the same position.
    if (tuple<int,int> (row_0, col_0)!=moves[moves.size()-1]){
        return false;
    }
    //Check if all moves are knight moves.
    vector<vector<int>> squares(rows,vector<int>(cols,0));
    for (int i = 0; i < moves.size() - 1; ++i) {
        t1_row=get<0>(moves[i]);
        t1_col=get<1>(moves[i]);
        if (squares[t1_row][t1_col]==1){
            return false;
        }
        t2_row=get<0>(moves[i+1]);
        t2_col=get<1>(moves[i+1]);

        if (abs(t2_row-t1_row)==2){
            if(abs(t2_col-t1_col)!=1){
                return false;
            }
        }else if(abs(t2_row-t1_row)==1){
            if(abs(t2_col-t1_col)!=2){
                return false;
            }
        } else{
            return false;
        }
    }
    t1_row=get<0>(moves[0]);
    t1_col=get<1>(moves[0]);
    if (squares[t1_row][t1_col]==1){
        return false;
    }
    //Check if the cycle is closed by a knight move.
    if (abs(row_0-t1_row)==2){
        if(abs(col_0-t1_col)!=1){
            return false;
        }
    }else if(abs(row_0-t1_row)==1){
        if(abs(col_0-t1_col)!=2){
            return false;
        }
    } else{
        return false;
    }

    return true;
}


bool test_kt(int rows, int cols, int row_0, int col_0) {
    vector<tuple<int,int>> res= KT(rows, cols, row_0, col_0);
    return test_kt_aux(rows, cols, row_0, col_0, res);
}