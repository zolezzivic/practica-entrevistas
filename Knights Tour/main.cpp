#include <iostream>
#include "test.h"
#include <chrono>

int main() {
    int worst_time=INT32_MIN;
    int worst_r0, worst_c0;
    int current_time;
    for (int i = 0; i < 8; ++i) {
        for (int j = 0; j < 8; ++j) {
            cout<< i<<", "<<j<<endl;
            auto start = chrono::steady_clock::now();
            bool res_test= test_kt(8, 8, i, j);
            auto end = chrono::steady_clock::now();
            if (!res_test){
                cout<<"Something is wrong"<<i<<j<<endl;
                return 1;
            }
            current_time= (int)(chrono::duration_cast<chrono::microseconds>(end - start).count());
            if (current_time>worst_time){
                worst_time=current_time;
                worst_r0=i;
                worst_c0=j;
            }
        }
    }
    cout<< "Worst execution time: "<<worst_time<<"ms"<<endl;
    cout << "Worst row0 and col0: "<<worst_r0<<", "<<worst_c0<<endl;


    for (int k = 8; k <40 ; k=k+2) {
        bool res_test= test_kt(k, k, 5, 5);
        cout<<"Size of board: "<<k<<"x"<<k<<endl;
        if(res_test){
            cout<< "The tests have been passed successfully"<<endl;
        }else{
            cout<< "The tests have failed"<<endl;
        }
    }
    return 0;

}
