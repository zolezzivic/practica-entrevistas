#ifndef KNIGHTS_TOUR_KNIGHTSTOUR_H
#define KNIGHTS_TOUR_KNIGHTSTOUR_H

#include <iostream>
#include <vector>
#include <tuple>
#include <algorithm>
using namespace std;

struct neigs_neig{
    tuple<int, int> neighbor;
    int number_of_neigh;
    neigs_neig(tuple<int, int> n, int n_n) : neighbor(n), number_of_neigh (n_n){}
    bool operator<(const struct neigs_neig& other) const{
        return number_of_neigh < other.number_of_neigh;
    }
};

vector<tuple<int, int>> KT(int rows, int cols, int row_0, int col_0);
int number_of_neighbors(int row, int col, int rows, int cols);
vector<tuple<int, int>> possible_neighbors(int row, int col, int rows, int cols);
void KT_aux(int row, int col, int rows, int cols, vector<tuple<int, int>> &res);
void print_vector(vector<tuple<int,int>>&);
bool reachable_squares(int visited_squares, int rows, int cols);



#endif //KNIGHTS_TOUR_KNIGHTSTOUR_H
