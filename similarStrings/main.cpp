#include <iostream>
#include <vector>
#include <unordered_set>
#include <string>

using namespace std;

void addToDicSimilarsOf(string& s, unordered_set<string>* dic){

    for(int i=0; i<s.size();i++){
        for(int j=i;j<s.size();j++){
            swap(s[i], s[j]);
            dic->insert(s);
            swap(s[i], s[j]);
        }
    }
}


bool checkIfSimilar(string& s1, string& s2){
    int numberOfDifferences=0;
    for(int i=0; i<s1.size();i++){
        if(s1[i]!=s2[i]) numberOfDifferences++;
        if(numberOfDifferences>2) return false;
    }
    return true;
}


void unite(vector<int> & parents, int a, int b){
    if(parents[a]==a) parents[a]=b;
    else if(parents[b]==b) parents[b]=a;
    else{
        while(parents[b]!=b){
            b=parents[b];
        }
        parents[b]=a;
    }

}

int numSimilarGroups(vector<string>& strs) {
    vector<int> parents(strs.size());
    for (int k = 0; k < parents.size(); ++k) {
        parents[k]=k;
    }

    for (int i = 0; i < strs.size(); ++i) {
        for (int j = i+1; j < strs.size(); ++j) {
            if(checkIfSimilar(strs[i], strs[j])){
                unite(parents, i, j);
            }
        }
    }

    unordered_set<int> uniqueParentes;
    for (int k = 0; k < parents.size(); ++k){
        if(parents[k]==k)uniqueParentes.insert(parents[k]);
    }
    return uniqueParentes.size();
}


int main() {
    vector<string> s={"kccomwcgcs","socgcmcwkc","sgckwcmcoc","coswcmcgkc","cowkccmsgc","cosgmccwkc","sgmkwcccoc","coswmccgkc","kowcccmsgc","kgcomwcccs"};
    s={"tars","rats","arts","star"};
    int groups= numSimilarGroups(s);
    std::cout << groups << std::endl;
    return 0;
}
