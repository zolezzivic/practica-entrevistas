#include <iostream>
#include <vector>
#include <math.h>
#include <algorithm>
using namespace std;


vector<bool> decimalToBinary(int n){
    if(n==0) return {0};
    vector<bool> res(floor(log2(n))+1, 0);
    int currentLog;
    while(n!=0){
        currentLog= floor(log2(n));
        res[currentLog]=1;
        n-=pow(2,currentLog);
    }
    reverse(res.begin(), res.end());
    return res;
}

int binaryToInt(vector<bool> &binaryNumber){
    int res=0;
    for (int i = binaryNumber.size()-1; i >=0 ; --i) {
        if(binaryNumber[i]) res+= pow(2,binaryNumber.size()-1-i);
    }
    return res;
}


int sumWithoutOperators(int A, int B){
    vector<bool> binaryA=decimalToBinary(A);
    vector<bool> binaryB=decimalToBinary(B);

    int minSize=min(binaryA.size(), binaryB.size());

    vector<bool> res;
    bool currentBitSum;
    bool carry=false;

    int idxA=binaryA.size()-1;
    int idxB=binaryB.size()-1;

    while(idxA>=0 && idxB>=0){
        currentBitSum=binaryA[idxA]^binaryB[idxB];
        currentBitSum= currentBitSum^carry;
        if(carry&binaryA[idxA] || carry&binaryB[idxB]) {
            carry=true;
        }
        else if(binaryA[idxA]&binaryB[idxB]) carry=true;
        else carry=false;
        res.push_back(currentBitSum);

        idxA--;
        idxB--;
    }

    while(idxA>=0){
        currentBitSum=binaryA[idxA];
        currentBitSum= currentBitSum^carry;
        if(carry&binaryA[idxA]) {
            carry=true;
        }else carry=false;
        res.push_back(currentBitSum);
        idxA--;
    }

    while(idxB>=0){
        currentBitSum=binaryB[idxB];
        currentBitSum= currentBitSum^carry;
        if(carry&binaryB[idxB]) {
            carry=true;
        }else carry=false;

        res.push_back(currentBitSum);
        idxB--;
    }

    if(carry) res.push_back(1);

    reverse(res.begin(), res.end());
    return binaryToInt(res);
}


int main() {

    for (int i = 0; i < 50; ++i) {
        int a= rand()%100;
        int b=rand()%200;
        if(a+b!=sumWithoutOperators(a,b)) printf("Salio mal con a: %i y b: %i\n", a, b);
    }

    std::cout << sumWithoutOperators(2,7) << std::endl;
    return 0;
}
