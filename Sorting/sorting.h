#ifndef SORTING_SORTING_H
#define SORTING_SORTING_H

#include <bits/stdc++.h>
#include <vector>

void insertionSort(std::vector<int>&);
void selectionSort(std::vector<int> &);
std::vector<int> mergeSort(std::vector<int> &, int, int);
void quickSort(std::vector<int> &, int, int);
void heapSort(std::vector<int> &);
bool binarySearch(std::vector<int> &, int);
bool isSorted(std::vector<int>&);
bool test();

#endif //SORTING_SORTING_H
