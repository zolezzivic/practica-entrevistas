#include "sorting.h"

void insertionSort(std::vector<int>& v){
    int j;
    for (int i = 1; i < v.size(); ++i) {
        j=i;
        while(j>0 && v[j-1]>v[j]){
            std::swap(v[j-1], v[j]);
            j--;
        }
    }
}

int getMin(std::vector<int>& v, int j){
    int min = INT32_MAX;
    int idx=-1;
    for (int i = j; i < v.size(); ++i) {
        if (v[i]<min){
            min=v[i];
            idx=i;
        }
    }
    return idx;
}

void selectionSort(std::vector<int>& v){
    for (int i = 0; i < v.size(); ++i) {
        for (int j = i; j < v.size(); ++j) {
            std::swap(v[getMin(v, i)], v[i]);
        }
    }
}

std::vector<int> mergeSort(std::vector<int> & v, int left, int right){
    if (right-left<=1){
        if (right==left) return {};
        return {v[left]};
    }
    int mid = (right+left)/2;
    std::vector<int> vL = mergeSort(v, left, mid);
    std::vector<int> vR = mergeSort(v, mid, right);
    int idxL=0;
    int idxR =0;
    int idx=0;
    std::vector<int> res;
    while(idxL<vL.size() && idxR<vR.size()){
        if (vL[idxL]<vR[idxR]){
            res.push_back(vL[idxL]);
            idxL++;
            idx++;
        }else{
            res.push_back(vR[idxR]);
            idxR++;
            idx++;
        }
    }
    while(idxL<vL.size()){
        res.push_back(vL[idxL]);
        idxL++;
        idx++;
    }
    while(idxR<vR.size()){
        res.push_back(vR[idxR]);
        idxR++;
        idx++;
    }
    return res;
}

int maxIdx(std::vector<int> & v, int left, int right){
    int res=INT32_MIN;
    int idx=-1;
    for (int i=left; i<right; i++) {
        if (v[i]>res) res=v[i], idx=i;
    }
    return idx;
}

void quickSort(std::vector<int> &v, int left, int right){
    if (right-left<=1) return;
    //We choose a random item of the vector as pivot.
    int pivotIdx = left+ rand() % (right-left);
    int pivot = v[pivotIdx];
    int upper=right-1;
    int lower =left+1;
    //We put the pivot element at the first position of the array
    std::swap(v[left], v[pivotIdx]);
    int maxIndex = maxIdx(v, left+1, right);
    //We put the max element (here we dont consider the pivot element) at the end of the vector. This will ensure lower wont be bigger than size-1
    std::swap(v[right-1], v[maxIndex]);
    while (upper >= lower){
        if (v[lower]<=pivot) lower++;
        else if (v[upper]>=pivot) upper--;
        else if (v[lower]>pivot && v[upper]<pivot) {
            std::swap(v[lower],v[upper]);
            lower++;
            upper--;
        }
    }
    //We put the pivot in its place (we swap it with a smaller element)
    std::swap(v[left], v[upper]);
    //We sort the elements smaller than the pivot and the bigger ones.
    quickSort(v, left, upper);
    quickSort(v, upper+1, right);

}

void siftDown (std::vector<int>& h, int i, int n){
    int idxMaxChild;
    int left=2*i + 1;
    int right=2*i + 2;
    if (i< n/2){
        if (left < n){
            if (right<n){
                idxMaxChild = (std::max(h[left], h[right])==h[left]) ? left:right;
            }else{
                idxMaxChild=left;
            }
        }else{
            idxMaxChild=right;
        }
        if (h[i]<h[idxMaxChild]){
            std::swap(h[i], h[idxMaxChild]);
            siftDown(h, idxMaxChild, n);
        }
    }
}

void heapify(std::vector<int>& v){
    for (int i = v.size()/2; i >=0 ; --i) {
        siftDown(v,i, v.size());
    }
}

void heapPop(std::vector<int>& v,  int size){
    v[0]=v[size];
    siftDown(v,0, size-1);
}

void heapSort(std::vector<int> &v){
    heapify(v);
    int n=v.size();
    int max;
    for (int i = n-1; i >=0; --i) {
        max=v[0];
        heapPop(v, i);
        v[i]=max;
    }
}




bool binarySearch(std::vector<int> &v, int elem){
    int r=0;
    int l=v.size();
    int mid;
    while (l>r){
        mid = (l+r)/2;
        if (v[mid]==elem) return true;
        else if (v[mid]>elem){
            l=mid;
        }else{
            r=mid+1;
        }
    }
    return false;
}





bool isSorted(std::vector<int>& v){
    for (int i = 0; i < v.size()-1; ++i) {
        if (v[i]>v[i+1]) return false;
    }
    return true;
}

bool linearSearch(std::vector<int>& v,int elem){
    for (int i = 0; i < v.size(); ++i) {
        if (v[i]==elem) return true;
    }
    return false;
}

bool test(){
    std::vector<int> v(500);
    for (int i = 0; i < 500; ++i) {
        v[i] = (rand() % 100);
    }
    insertionSort(v);
    if (! isSorted(v)) return false;
    for (int i = 0; i < 500; ++i) {
        v[i] = (rand() % 100);
    }
    selectionSort(v);
    if (! isSorted(v)) return false;
    int elem;
    for (int j = 0; j < 500; ++j) {
        elem = (rand() % 100);
        if (binarySearch(v, elem)!=linearSearch(v,elem)) return false;
    }
    for (int i = 0; i < 500; ++i) {
        v[i] = (rand() % 100);
    }
    quickSort(v, 0, v.size());
    if (!isSorted(v)) return false;
    for (int i = 0; i < 500; ++i) {
        v[i] = (rand() % 100);
    }
    heapSort(v);
    if (!isSorted(v)) return false;

    for (int i = 0; i < 500; ++i) {
        v[i] = (rand() % 100);
    }
    std::vector<int> sort( mergeSort(v, 0, v.size()));
    return isSorted(sort);
}