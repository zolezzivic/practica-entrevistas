#include <iostream>
#include <unordered_map>

#include <vector>
#include <string>
using namespace std;

unordered_map<char, vector<char>> numsLetters;



vector<string> letterCombinationsAux(string digits) {
    if(digits.empty()) return {};

    char currentDigit= digits[digits.size()-1];
    digits.pop_back();
    vector<string> resWithoutCurrentDigit= letterCombinationsAux(digits);

    vector<string> res;
    for(string& s: resWithoutCurrentDigit){
        for(char letter: numsLetters[currentDigit]){
            res.push_back(s + letter);
        }
    }

    if(resWithoutCurrentDigit.empty()){
        for(char letter: numsLetters[currentDigit]){
            res.push_back({letter});
        }
    }

    return res;
}

int main() {

    numsLetters.insert({'2', {'a','b','c'}});
    numsLetters.insert({'3', {'d','e','f'}});
    numsLetters.insert({'4', {'g','h','i'}});
    numsLetters.insert({'5', {'j','k','l'}});
    numsLetters.insert({'6', {'m','n','o'}});
    numsLetters.insert({'7', {'p','q','r','s'}});
    numsLetters.insert({'8', {'t','u','v'}});
    numsLetters.insert({'9', {'w','x','y','z'}});


    vector<string> res= letterCombinationsAux("23");
    std::cout << "Hello, World!" << std::endl;
    return 0;
}
