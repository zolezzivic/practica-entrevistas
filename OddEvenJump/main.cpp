#include <iostream>
#include <vector>

int oddEvenJumps(std::vector<int>& arr);
std::pair<int,bool> oddEvenJumpsPD(std::vector<int>& arr, int fstIdx, bool isOddJump, std::vector<std::vector<int>>&);


int main() {
    std::vector<int> a={10,13,12,14,15};
    std::cout << oddEvenJumps(a)<< std::endl;
    return 0;
}

int oddEvenJumps(std::vector<int>& arr){
    std::vector<std::vector<int>> goodIndeces(arr.size(),std::vector<int>(2,-1));
    return oddEvenJumpsPD(arr, 0, true, goodIndeces).first;
}

std::pair<int,bool> oddEvenJumpsPD(std::vector<int>& arr, int fstIdx,bool isOddJump, std::vector<std::vector<int>>& goodIndeces){
    if(fstIdx>=arr.size()-1) return std::make_pair(1,true);
    if(goodIndeces[fstIdx][isOddJump]==-1){
        int res=0;
        for (int i = fstIdx+1; i < arr.size(); ++i) {
            std::pair<int,bool> aux=oddEvenJumpsPD(arr, i, isOddJump, goodIndeces);
            if(aux.second) res+=aux.first;
        }
        int min=INT32_MAX;
        int minIdx=-1;
        int max=INT32_MIN;
        int maxIdx=-1;
        for (int i = fstIdx+1; i < arr.size(); ++i) {
            if(arr[i]>=arr[fstIdx] && arr[i]<min){
                min=arr[i];
                minIdx=i;
            }else if(arr[i]<=arr[fstIdx] && arr[i]>max){
                max=arr[i];
                maxIdx=i;
            }
        }
        bool isGoodIdx=false;
        if(isOddJump && minIdx>fstIdx){
            std::pair<int,bool> evenJump= oddEvenJumpsPD(arr, minIdx, false);
            if(evenJump.second) {
                res+= 1;
                isGoodIdx=true;
            }
        }
        if(!isOddJump && maxIdx>fstIdx){
            std::pair<int,int> oddJump= oddEvenJumpsPD(arr, maxIdx, true);
            if(oddJump.second){
                res+=1;
                isGoodIdx=true;
            }
        }
    }
    return std::make_pair(res, isGoodIdx);
}
