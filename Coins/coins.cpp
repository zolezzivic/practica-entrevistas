#include "coins.hpp"

//To solve this problem I will use a Backtracking algorithm.
//I will start by taking one coin at a time, coin that must be greater or equal to the last coin we have chosen in order to
//avoid considering two times the same solution. If the sum of the chosen coins is less than 200, we keep choosing coins,
//if it is bigger than 200, we backtrack, and if it is equal to 200, we add one to the current number of found solutions.

int ways=0;
int totalSum=200;
std::vector<int> coins = {1,2,5,10,20,50,100,200};
std::vector<std::vector<int>> memo;


void numberOfWaysBTAux(int lastCoin, int sumCoins){
    if (sumCoins>totalSum){
        return;
    }
    if (sumCoins==totalSum){
        ways++;
        return;
    }
    for (int coin: coins) {
        if (coin>= lastCoin){
            numberOfWaysBTAux(coin, sumCoins+coin);
        }
    }
}

int numberOfWaysBT(){
    ways=0;
    numberOfWaysBTAux(-1, 0);
    return ways;
}

//PD version: complexity = O(8*200 * 8) = O(nCoins^2 * totalSum)= O(totalSum);

int numberOfWaysPDAux(int lastCoin, int n){
    if (n<0) return 0 ;
    if (n==0) return 1;
    if (memo[n][lastCoin]==-1){
        int aux=0;
        for(int i=lastCoin;i<coins.size();i++){
            aux+=numberOfWaysPDAux(i, n-coins[i]);
        }
        memo[n][lastCoin]=aux;
    }
    return memo[n][lastCoin];
}

int numberOfWaysPD(){
    memo=std::vector<std::vector<int>> ((totalSum+1), std::vector<int>(coins.size(), -1));
    return numberOfWaysPDAux(0, totalSum);
}


