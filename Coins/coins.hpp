#ifndef COINS_COINS_HPP
#define COINS_COINS_HPP
#include <vector>

void numberOfWaysBTAux(int lastCoin, int sumCoins);
int numberOfWaysBT();
int numberOfWaysPD();
int numberOfWaysPDAux(int lastCoin, int sumCoins);

#endif //COINS_COINS_HPP
