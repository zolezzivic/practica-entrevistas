#include <bits/stdc++.h>
#include <chrono>
#include "coins.hpp"

int main() {
    auto start = std::chrono::steady_clock::now();
    int res =numberOfWaysPD();
    auto end = std::chrono::steady_clock::now();
    long time = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();

    printf("Number of ways: %d\n", res);
    printf("Execution time: %ld ms\n", time);
    return 0;
}
