#include<stdio.h>
#include<stdlib.h>
#include<string.h>

void strrev (char* s);
void wordrev (char* s);
void strrev2 (char* s, int from, int to);
int testStrRev(int);
int testWordRev(int sizeOfString);


int main(){
	int test = testStrRev(1000);
	if (test==1) printf("strRev's test have been passed\n");
	else printf ("strRev is not working correctly\n");

	int testW = testWordRev(300);
	if (testW==1) printf("wordRev's test have been passed\n");
	else printf ("wordRev is not working correctly\n");

	return 0;
}

void strrev (char* s){
	int sSize = strlen(s);
	char aux='a';
	for (int i = 0; i < sSize/2; ++i)	{
		aux=s[i];
		s[i]=s[sSize-1-i];
		s[sSize-1-i]=aux;
	}
}

void strrev2 (char* s, int from, int to){
	int sSize = strlen(s);
	char aux='a';
	for (int i = from; i < (to+from)/2; ++i)	{
		aux=s[i];
		s[i]=s[to-1-(i-from)];
		s[to-1-(i-from)]=aux;
	}
}

void wordrev(char* s){
	int sSize = strlen(s);
	strrev(s);
	char aux;
	int start=0;
	int end = 0;
	int i=0;
	while (end< sSize){
		//Look for word -> start to end
		while( end<sSize && s[end]!=' ' ) {
			end++;
		}

		strrev2(s, start, end);
		end++;
		start=end;
	}
}



int testStrRev(int sizeOfString){
	char* s = (char*)malloc(sizeOfString+1);
	char* original = (char*)malloc(sizeOfString+1);
	//Create random string
	for (int i = 0; i < sizeOfString; ++i){
		s[i] =  (char)('a' + (random() % 26));
	}
	s[sizeOfString]=0;
	strcpy(original, s);
	strrev(s);

	for (int i = 0; i < sizeOfString; ++i){
		if (s[i] != original[sizeOfString-1-i]) return 0;
	}

	free(s);
	free(original);
	return 1;
}

int testWordRev(int sizeOfString){
	char* s= (char*)malloc(sizeOfString+1);
	char* original= (char*)malloc(sizeOfString+1);
	for (int i = 0; i < sizeOfString; ++i){
		if (i >0 && i % (random() % 10 +1) == 0){
			s[i]=' ';
			original[i]=' ';

		}else{
			s[i] =  (char)('a' + (random() % 26));
			original[i]=s[i];
		}
	}
	s[sizeOfString]=0;
	original[sizeOfString]=0;
	wordrev(s);
	int start=0;
	int end=0;
	while (start < sizeOfString-1 &&  end < sizeOfString){
		while (end< sizeOfString && s[end]!= ' '){
			end++;
		}
		for (int i = 0; i < end-start; ++i){	
			if (s[i+start] != original[sizeOfString-end+i]){
				free(s);
				free(original);
				return 0;	
			} 
		}
		end++;
		start = end;
		
	}
	
	free(s);
	free(original);
	return 1;

}


/*
Not inplace algorithms
void strrev (char* s){
	int sSize = 0;
	while (s[sSize]!=0){
		sSize++;
	}
	char* res = (char*)malloc(sSize+1);
	for (int i = 0; i < sSize; ++i)	{
		res[i]=s[sSize-1-i];
	}	
	res[sSize]=0;
	return res;
}

char* wordrev(char* s){
	int sSize = strlen(s);
	char* res = (char*) malloc(sSize+1);
	int start=sSize-1;
	int end = sSize-1;
	int i=0;
	while (start>=0){
		//Look for word -> start to end
		while( start>=0 && s[start]!=' ' ) {
			start--;
		}
		//Skip the space
		start++;
		//Rewrite the word in the solution string
		for (int j = start; j <= end; ++j){
			res[i]=s[j];
			i++;
		}
		//Write the space if we are not at the end of the string
		if (i<sSize) {
			res[i]=' ';
			i++;
		}
		//Update variables
		start-=2;
		end=start;	
	}
	//Write a zero at the end of the string
	res[sSize]=0;
	return res;			
}
*/