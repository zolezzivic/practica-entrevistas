#include <iostream>
#include <string>
#include <vector>
#include <queue>
#include <math.h>
#include <unordered_set>

using namespace std;

struct distanceAndPoint{
    float distance;
    int pointIdx;
    distanceAndPoint(): distance(-1), pointIdx(-1){}
    distanceAndPoint(float d, int p): distance(d), pointIdx(p) {}
    bool operator < (const struct distanceAndPoint& other) const{
        return (distance<other.distance);
    }
};

template<class T> void printVector(vector<T>& v){
    for (T elem:v) cout<<elem<<",";
    printf("\n");
}


struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

struct coordinate{
    int x;
    int y;
    coordinate(): x(0), y(0){};
    coordinate(int x, int y): x(x), y(y){};
    bool operator < (const struct coordinate other) const {
        if(x<other.x) return true;
        if(x==other.x) return y<other.y;
        return false;
    }
    bool operator == (const struct coordinate other) const{
        if(x!=other.x) return false;
        return y == other.y;
    }
};


//------------------------------------------------------------------------------

bool existBacktracking(vector<vector<char>>& board, string &word, int currentLetter, coordinate currentPos){
    if(currentLetter>=word.size()) return true;
    if(currentPos.x<0 || currentPos.y<0 || currentPos.x>=board.size() || currentPos.y>=board[0].size()) return false;
    if(board[currentPos.x][currentPos.y]!=word[currentLetter]) return false;
    board[currentPos.x][currentPos.y]='.';
    coordinate neighbor=currentPos;
    neighbor.x+=1;
    if(existBacktracking(board, word, currentLetter+1, neighbor)) return true;
    neighbor=currentPos;
    neighbor.y+=1;
    if(existBacktracking(board, word, currentLetter+1, neighbor)) return true;
    neighbor=currentPos;
    neighbor.y-=1;
    if(existBacktracking(board, word, currentLetter+1, neighbor)) return true;
    neighbor=currentPos;
    neighbor.x-=1;
    if(existBacktracking(board, word, currentLetter+1, neighbor)) return true;

    board[currentPos.x][currentPos.y]=word[currentLetter];
    return false;
}

bool exist(vector<vector<char>>& board, string word) {
    for (int row = 0; row < board.size(); ++row) {
        for (int col = 0; col < board[row].size(); ++col) {
            if(existBacktracking(board, word, 0, coordinate(row,col))) return true;
        }
    }
    return false;
}



int main() {
    vector<vector<char>> board = {{'A','B','C','E'},
                                  {'S','F','C','S'},
                                  {'A','D','E','E'}};
    //vector<vector<char>> board = {{'A','B'},{'S','F'},{'A','D'}};
    string word = "ABCB";


    printf("%i\n", exist(board, word));

    return 0;
}

