#include <iostream>

struct TreeNode {
     int val;
     TreeNode *left;
     TreeNode *right;
     TreeNode() : val(0), left(nullptr), right(nullptr) {}
     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

struct heightAndMaxPath{
    int height;
    int path;
    heightAndMaxPath(int h, int p): height(h), path(p) {};
};

heightAndMaxPath diameterAndHeightOfBT(TreeNode* root){
    if(root== nullptr) return heightAndMaxPath(0,0);
    heightAndMaxPath left=heightAndMaxPath(0,0);
    heightAndMaxPath right=heightAndMaxPath(0,0);
    if(root->left!= nullptr) left=diameterAndHeightOfBT(root->left);
    if(root->right!= nullptr) right=diameterAndHeightOfBT(root->right);

    int rootHeight=std::max(left.height, right.height);
    if(root->left!= nullptr || root->right!= nullptr) rootHeight++;
    int maxPathBetweenRightAndLeft=std::max(left.path, right.path);
    int pathWithRoot=0;
    if(root->right!= nullptr) pathWithRoot=pathWithRoot+1+right.height;
    if(root->left!= nullptr) pathWithRoot=pathWithRoot+1+left.height;


    heightAndMaxPath res= heightAndMaxPath(rootHeight, std::max(pathWithRoot, maxPathBetweenRightAndLeft));
    return res;
}

int diameterOfBinaryTree(TreeNode* root) {
    return diameterAndHeightOfBT(root).path;
}


int main() {
    //TreeNode tree= TreeNode(1, new TreeNode(2), new TreeNode(5));
    TreeNode tree= TreeNode(1, new TreeNode(2, new TreeNode(4), new TreeNode(5)), new TreeNode(3));
    std::cout << diameterOfBinaryTree(&tree) << std::endl;
    return 0;
}
