#include <iostream>
#include <vector>

int minDecreasingSubsequence(std::vector<int>&);


int main() {
    std::vector<int> v={2, 9, 12, 13, 4, 7, 6, 5, 10};
    std::cout << minDecreasingSubsequence(v) << std::endl;
    return 0;
}


int minDecreasingSubsequence(std::vector<int>&v){
    std::vector<int> headOfSubsequences;
    headOfSubsequences.push_back(v[v.size()-1]);
    int selectedSubsequence;
    int difference;
    for (int i = v.size()-2; i >=0; --i) {
        selectedSubsequence=-1;
        difference=INT32_MAX;
        for (int j = headOfSubsequences.size()-1; j >=0; --j) {
            if(headOfSubsequences[j]<v[i]){
                if(difference==INT32_MAX || difference> v[i]-headOfSubsequences[j]){
                    difference=v[i]-headOfSubsequences[j];
                    selectedSubsequence=j;
                }
            }
        }
        if(selectedSubsequence!=-1) headOfSubsequences[selectedSubsequence]=v[i];
        else headOfSubsequences.push_back(v[i]);
    }
    return headOfSubsequences.size();
}
