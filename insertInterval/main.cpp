#include <iostream>
#include<vector>

using namespace std;


vector<vector<int>> insert(vector<vector<int>>& intervals, vector<int>& newInterval) {
    if(newInterval.empty()) return intervals;
    if(intervals.empty()) return {newInterval};
    vector<vector<int>> res;

    int idxFirstInterval=0;
    int idxSecondInterval=0;

    while(idxFirstInterval<intervals.size() && intervals[idxFirstInterval][1]<newInterval[0]){
        res.push_back(intervals[idxFirstInterval]);
        idxFirstInterval++;
    }
    idxSecondInterval=idxFirstInterval;
    vector<int> intervalToInsert= newInterval;
    if(idxFirstInterval< intervals.size() && newInterval[1]>= intervals[idxFirstInterval][0]){
        intervalToInsert[0]=min(intervals[idxFirstInterval][0], newInterval[0]);
        intervalToInsert[1]=max(intervals[idxFirstInterval][1], newInterval[1]);
        idxSecondInterval=idxFirstInterval+1;
    }


    while(idxSecondInterval<intervals.size() && intervalToInsert[1]>=intervals[idxSecondInterval][0]){
        intervalToInsert[1]=max(intervals[idxSecondInterval][1], intervalToInsert[1]);
        idxSecondInterval++;
    }
    res.push_back(intervalToInsert);

    for(int i=idxSecondInterval; i<intervals.size(); i++) res.push_back(intervals[i]);

    return res;


}
int main() {
    vector<vector<int>> intervals={{1,2},{3,5},{6,7},{8,10},{12,16}};
    vector<int> newInt={4,8};
    vector<vector<int>> res=insert(intervals, newInt);
    for(auto v: res) printf("[%i %i] ", v[0], v[1]);
    printf("\n");

    return 0;
}
