#include <iostream>
#include <bits/stdc++.h>
#include <vector>
#include<unordered_map>
#include <queue>

using namespace std;


int minLengthSubstring(string s, string t) {
    unordered_map<char, vector<int>> lettersPositions;
    for(int i=0; i<s.size();i++) lettersPositions[s[i]].push_back(i);
    unordered_map<char,vector<int>> positions;
    unordered_map<char,int> appearances;
    unordered_map<char, int> indices;

    for(int i=0;i<t.size();i++){
        if(lettersPositions.count(t[i])) {
            if (!positions.count(t[i])) {
                positions[t[i]] = lettersPositions[t[i]];
                indices[t[i]] = 0;
            }
            appearances[t[i]]++;
            lettersPositions[t[i]].pop_back();
            if(lettersPositions[t[i]].empty()) lettersPositions.erase(t[i]);
        } else return -1;
    }

    queue<tuple<int, char>> minAppearances;
    for (auto c:positions) {
        char currentLetter=c.first;
        if(appearances[currentLetter]<positions[currentLetter].size()-indices[currentLetter]){
            minAppearances.push({positions[currentLetter][indices[currentLetter]],currentLetter});
        }
    }
    while(!minAppearances.empty()){
        char currentLetter=get<1>(minAppearances.front());
        indices[currentLetter]++;
        int newIdx=indices[currentLetter];
        minAppearances.pop();
        if(appearances[currentLetter]<positions[currentLetter].size()-indices[currentLetter]){
            minAppearances.push({positions[currentLetter][newIdx],currentLetter});
        }
    }

    int maxIdx=-1;
    int minIdx=INT32_MAX;
    int position;
    for(auto c:positions){
        position=c.second[indices[c.first]];
        if(position>maxIdx) maxIdx=position;
        if(position<minIdx) minIdx=position;
    }
    printf("Desde %i hasta %i\n", minIdx,maxIdx);
    return maxIdx-minIdx+1;
}

int main() {
    string s="acabcd";
    string t="ac";
    std::cout << minLengthSubstring(s,t) << std::endl;
    return 0;
}
