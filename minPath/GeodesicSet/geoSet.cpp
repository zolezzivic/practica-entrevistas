#include <iostream>
#include <vector>
#include <queue>
#include <sstream>


std::vector<int> BFS(std::vector<std::vector<int>>&, int);
int getCloserNode(std::vector<int>, std::vector<int>);
bool isGeodetic(std::vector<std::vector<int>>& adj, std::vector<int> D);

int main() {
    int N;
    std::string v;
    scanf("%d", &N);
    std::vector<std::vector<int>> adj(N, std::vector<int>(N, 0));
    std::string line;
    std::getline(std::cin, line);
    for (int i = 0; i < N; ++i) {
        std::string line;
        std::getline(std::cin, line);
        std::istringstream iss(line);

        while (iss >> v) {
            int n =std::stoi(v) - 1;
            adj[i][n]=1;
        }
    }

    int numberOfSets;
    scanf("%d", &numberOfSets);
    std::string line1;
    std::getline(std::cin, line1);
    for (int i = 0; i < numberOfSets; ++i) {
        std::vector<int> D;
        std::string line1;
        std::getline(std::cin, line1);
        std::istringstream iss(line1);

        while (iss >> v) {
            D.push_back(std::stoi(v) - 1);
        }
        if (isGeodetic(adj, D)){
            printf("yes\n");
        }else{
            printf("no\n");
        }
    }
    return 0;
}



std::vector<int> BFS(std::vector<std::vector<int>>& adj, int s){
    std::vector<int> distances(adj.size(), INT32_MAX);
    std::queue<int> q;
    q.push(s);
    distances[s]=0;
    int node;
    while (! q.empty()){
        node = q.front();
        q.pop();
        for (int i = 0; i < adj.size(); ++i) {
            if (adj[node][i] && distances[i]== INT32_MAX){
                q.push(i);
                distances[i]=distances[node]+1;
            }
        }

    }
    return distances;
}

void transpose(std::vector<std::vector<int>>& m){
    int aux;
    for (int i = 0; i < m.size(); ++i) {
        for (int j = 0; j < m[0].size(); ++j) {
            aux=m[i][j];
            m[i][j]=m[j][i];
            m[j][i]=m[i][j];
        }
    }
}

bool isGeodetic(std::vector<std::vector<int>>& adj, std::vector<int> D){
    std::vector<std::vector<int>> BFSs(adj.size());
    for (int i = 0; i < adj.size(); ++i) {
        BFSs[i] =BFS(adj, i);
    }
    std::vector<std::vector<int>> reverseBFS(adj.size());
    transpose(adj);
    for (int i = 0; i < adj.size(); ++i) {
        reverseBFS[i]=BFS(adj, i);
    }
    std::vector<int> geodesicNodes;
    std::vector<int> visitedNodes(adj.size(), 0);
    for (int node1 : D) {
        for (int node2 : D) {
            for (int i = 0; i < adj.size(); ++i) {
                int sum =BFSs[node1][i] +reverseBFS[i][node2];
                if (sum == BFSs[node1][node2] && visitedNodes[i]==0) {
                    geodesicNodes.push_back(i);
                    visitedNodes[i]=1;
                }
            }
        }
    }
    return geodesicNodes.size()==adj.size();
}