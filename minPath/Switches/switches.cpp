#include <bits/stdc++.h>
#include <vector>
#include <unordered_set>
#include <cmath>
#include <queue>
#include <list>


int binToDec(std::vector<bool> &n);
int BFS(int N, std::vector<int> &switches);
void numberOfSwitches(int N, std::vector<int> &switches, int caseNumber);

int main() {
    int TC, N,M;
    scanf("%d", &TC);

    for (int i = 0; i < TC; ++i) {
        scanf("%d", &N);
        scanf("%d", &M);
        std::vector<int> switches(M);
        for (int j = 0; j < M; ++j) {
            std::vector<bool> binSwitch;
            int bit;
            for (int k = 0; k < N; ++k) {
                scanf("%d", &bit);
                binSwitch.push_back(bit == 1);
            }
            switches[j]=binToDec(binSwitch);
        }
        numberOfSwitches(N, switches, i+1);
    }
    return 0;
}


int binToDec(std::vector<bool>& n){
    int res = 0;
    for (int i = 0; i < n.size() ; ++i) {
        res += n[i] * std::pow(2, n.size()-1-i);
    }
    return res;
}




int BFS(int N, std::vector<int> &switches) {
    int first = std::pow(2, N) - 1;
    int last = 0;
    std::vector<int> distances(std::pow(2, N), INT32_MAX);
    std::unordered_set<int> visitedNodes;
    std::queue<int> q;
    q.push(first);
    visitedNodes.insert(first);
    distances[first]=0;
    int neighbor;
    while (!q.empty()){
        for (int s : switches) {
            neighbor = (q.front() ^ s);
            if (! visitedNodes.count(neighbor)){
                distances[neighbor] = distances[q.front()]+1;
                if (neighbor == last) break;
                visitedNodes.insert(neighbor);
                q.push(neighbor);
            }
        }
        if (neighbor == last) break;
        q.pop();
    }
    return distances[last];
}


void numberOfSwitches(int N, std::vector<int> &switches, int caseNumber) {
    int res= BFS(N, switches);
    if (res==INT32_MAX){
        printf("Case %d: IMPOSSIBLE\n", caseNumber);
    }else{
        printf("Case %d: %d\n", caseNumber, res);
    }
}