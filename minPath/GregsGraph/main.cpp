#include <iostream>
#include<bits/stdc++.h>
#include <vector>
#include <unordered_set>

std::vector<int> gregsGraph(std::vector<int>& deletedNodes, std::vector<std::vector<int>> &ady);
bool test();
int main() {
    int n;
    scanf("%d",&n);
    std::vector<std::vector<int>> ady(n, std::vector<int>(n, INT32_MAX));
    int current;
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            scanf("%d",&current);
            ady[i][j]=current;
        }
    }
    std::vector<int> deletedNodes(n);
    for (int k = 0; k < n; ++k) {
        scanf("%d",&current);
        deletedNodes[k]=current;
    }
    std::vector<int> res= gregsGraph(deletedNodes, ady);
    for(int i:res) printf("%d ", i);
    printf("\n");
    printf("Los tests salieron: %d\n", test());
    return 0;
}

std::vector<int> gregsGraph(std::vector<int>& deletedNodes, std::vector<std::vector<int>> &ady){
    int n=ady.size();
    std::vector<std::vector<int>> distances(n, std::vector<int>(n, INT32_MAX));
    for (int j = 0; j < n; ++j) {
        distances[j][j]=0;
    }
    std::unordered_set<int> currentNodes;
    std::vector<int> res(n, 0); //res[n-1]=0 because the min path between a node and itself has lenght 0
    currentNodes.insert(deletedNodes[n-1]);
    int sumOfMinPaths=0;
    for (int i = 1; i < n; ++i) {
        int newNode= deletedNodes[n-1-i]-1;
        //Adding the shortest path from the nodes to the new one
        for (int node1:currentNodes) {
            for(int node2:currentNodes){
                distances[node1-1][newNode]=std::min(distances[node1-1][newNode],distances[node1-1][node2-1]+ady[node2-1][newNode]);
            }
            sumOfMinPaths+=distances[node1-1][newNode];
        }
        //Adding the shortest path from the new node to the old ones
        for(int node1: currentNodes){
            for(int node2: currentNodes){
                distances[newNode][node1-1]=std::min(distances[newNode][node1-1], ady[newNode][node2-1]+distances[node2-1][node1-1]);
            }
            sumOfMinPaths+=distances[newNode][node1-1];
        }

        //Update of the shortest path between the old nodes
        for(int node1:currentNodes){
            for(int node2:currentNodes){
                int oldDistance=distances[node1-1][node2-1];
                distances[node1-1][node2-1]=std::min(distances[node1-1][node2-1], distances[node1-1][newNode]+distances[newNode][node2-1]);
                if(distances[node1-1][node2-1]!=oldDistance){
                    sumOfMinPaths-=oldDistance;
                    sumOfMinPaths+=distances[node1-1][node2-1];
                }
            }
        }
        currentNodes.insert(newNode+1);
        res[n-1-i]=sumOfMinPaths;
    }
    return res;

}


std::vector<std::vector<int>> Floyd(std::vector<std::vector<int>> &ady){
    int n=ady.size();
    std::vector<std::vector<int>> distances(n, std::vector<int>(n,INT32_MAX));
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            if(i==j&&distances[i][i]!=INT32_MAX) distances[i][i]=0;
            else if(ady[i][j]!=INT32_MAX) distances[i][j]=ady[i][j];
        }
    }
    for (int k = 0; k < n; ++k) {
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                if(distances[i][k]!=INT32_MAX && distances[k][j]!=INT32_MAX){
                    distances[i][j]=std::min(distances[i][j], distances[i][k]+distances[k][j]);
                }
            }
        }
    }
    return distances;
}

std::vector<int> bruteForce(std::vector<int>& deletedNodes, std::vector<std::vector<int>> &ady){
    std::vector<int> res(ady.size(), INT32_MAX);
    std::vector<std::vector<int>> currentDistances;
    for (int i = 0; i < ady.size(); ++i) {
        currentDistances=Floyd(ady);
        int currentSum=0;
        for (int j = 0; j < currentDistances.size(); ++j) {
            for (int k = 0; k < currentDistances[0].size(); ++k) {
                if(currentDistances[j][k]!=INT32_MAX) currentSum+=currentDistances[j][k];
            }
        }
        res[i]=currentSum;
        for (int l = 0; l < ady.size(); ++l) {
            ady[deletedNodes[i]-1][l]=INT32_MAX;
            ady[l][deletedNodes[i]-1]=INT32_MAX;
        }
    }
    return res;
}

bool test(){
    std::vector<std::vector<int>> ady;
    std::vector<int> deletedNodes;
    std::unordered_set<int> selectedNodesToDelete;
    for (int i = 1; i < 50; ++i) {
        selectedNodesToDelete.clear();
        ady=std::vector<std::vector<int>>(i, std::vector<int>(i, INT32_MAX));
        deletedNodes=std::vector<int>(i);
        for (int j = 0; j < i; ++j) {
            for (int k = 0; k < i; ++k) {
                ady[j][k]=rand()%400;
            }
            ady[j][j]=0;
            int selectedNode=rand()%i+1;
            while(selectedNodesToDelete.count(selectedNode)){
                selectedNode=rand()%i+1;
            }
            selectedNodesToDelete.insert(selectedNode);
            deletedNodes[j]=selectedNode;
        }
        std::vector<int> gregs=gregsGraph(deletedNodes, ady);
        std::vector<int> fb=bruteForce(deletedNodes, ady);

        if(gregs!=fb) {
            return false;
        }
    }
    return true;
}