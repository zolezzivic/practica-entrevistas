#include <iostream>
#include <vector>
#include <sstream>

int numberOfMice (std::vector<std::vector<int>>& maze, int time, int exitCell);
std::vector<int> dijkstra(std::vector<std::vector<int>>& maze, int exitCell);
void transpose(std::vector<std::vector<int>>& maze);

int main() {
    int TC,N, E, T, M, cell1, cell2, time;
    scanf("%d", &TC);


    for (int i = 0; i < TC; ++i) {
        //scanf("%d\n,%d\n,%d\n,%d\n", &N, &E, &T, &M);
        scanf("%d", &N);
        scanf("%d", &E);
        scanf("%d", &T);
        scanf("%d", &M);

        std::vector<std::vector<int>> maze(N, std::vector<int>(N, INT32_MAX));
        for (int j = 0; j < M; ++j) {
            scanf("%d", &cell1);
            scanf("%d", &cell2);
            scanf("%d", &time);
            maze[cell1-1][cell2-1]=time;
        }
        for (int j = 0; j < N; ++j) {
            maze[j][j]=0;
        }
        printf("%d\n", numberOfMice(maze, T, E-1));
        if (i < TC-1) printf("\n");
    }

    return 0;
}

void transpose(std::vector<std::vector<int>>& maze){
    int aux;
    for (int i = 0; i < maze.size(); ++i) {
        for (int j = 0; j < i; ++j) {
            aux = maze[i][j];
            maze[i][j]=maze[j][i];
            maze[j][i] = aux;
        }
    }
}

int getNextMouse(std::vector<int>& distances, std::vector<bool>& visitedMice){
    int min = INT32_MAX;
    int minMouse = -1;
    for (int i = 0; i < distances.size(); ++i) {
        if (!visitedMice[i] && distances[i]< min){
            min = distances[i];
            minMouse=i;
        }
    }
    return minMouse;
}

std::vector<int> dijkstra(std::vector<std::vector<int>>& maze, int exitCell){
    std::vector<int> distances(maze.size(), INT32_MAX);
    std::vector<bool> visitedMice(maze.size(), false);
    distances[exitCell]=0;
    visitedMice[exitCell]= true;

    for (int k = 0; k < maze.size(); ++k) {
        if (maze[exitCell][k]!= INT32_MAX) distances[k] = maze[exitCell][k];
    }

    for (int i = 0; i < maze.size(); ++i) {
        int mouse = getNextMouse(distances, visitedMice);
        if (mouse==-1) break;
        visitedMice[mouse]=true;
        for (int j = 0; j < maze.size(); ++j) {
            if (!visitedMice[j]){
                long sum = (long)(distances[mouse]) + (long)(maze[mouse][j]);
                if (distances[j]>sum) distances[j]=sum;
            }
        }
    }
    return distances;
}



int numberOfMice (std::vector<std::vector<int>>& maze, int time, int exitCell){
    int res=0;
    transpose(maze);
    std::vector<int> escapeTimes = dijkstra(maze, exitCell);
    for (int i = 0; i < escapeTimes.size(); ++i) {
        if (escapeTimes[i]<=time) res++;
    }
    return res;
}
