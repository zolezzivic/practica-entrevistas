#include <bits/stdc++.h>
#include <vector>
#include <algorithm>
#include <tuple>
#include <list>

std::vector<int> dijkstra(std::vector<std::vector<int>> &,  int);
int getNextCloserNode(std::vector<int> &, std::vector<int>);
int almostShortestPath(std::vector<std::vector<int>> &,  int, int);


int main() {
    int  N,M, S, D;
    scanf( "%d", &N);
    scanf( "%d", &M);
    while (N!=0 || M!=0){
        scanf( "%d", &S);
        scanf( "%d", &D);
        std::vector<std::vector<int>> adj(N, std::vector<int>(N, INT32_MAX));
        int node1, node2, dist;
        for (int i = 0; i < M; ++i) {
            scanf( "%d", &node1);
            scanf( "%d", &node2);
            scanf( "%d", &dist);
            adj[node1][node2]=dist;
        }
        for (int i=0; i<N; i++){
            adj[i][i]=0;
        }

        int res = almostShortestPath(adj, S, D);
        printf("%d\n", res);
        scanf( "%d", &N);
        scanf( "%d", &M);
    }

    return 0;
}




int getNextCloserNode(std::vector<int> &distances, std::vector<int> visitedNodes){
    int min=INT32_MAX;
    int closerNode =-1;
    for (int i = 0; i < distances.size(); ++i) {
        if (distances[i]<min && visitedNodes[i]==0){
            closerNode=i;
            min =distances[i];
        }
    }
    return closerNode;
}

std::vector<int> dijkstra(std::vector<std::vector<int>> & adj,  int s){
    int n=adj.size();
    std::vector<int> distances(n, INT32_MAX);
    std::vector<int> visitedNodes(n, 0);
    distances[s]=0;
    visitedNodes[s]=1;
    for (int i = 0; i < n; ++i) {
        distances[i]=adj[s][i];
    }

    for (int i = 0; i < n; ++i) {
        int node= getNextCloserNode(distances, visitedNodes);
        if (node ==-1) break;
        visitedNodes[node]=1;
        for (int j = 0; j < n; ++j) {
          long sum = (long)(distances[node])+(long)(adj[node][j]);
          distances[j] = std::min((long) (distances[j]), sum);
        }
    }
    return distances;
}

void transpose(std::vector<std::vector<int>> &adj){
    int aux;
    for (int i = 0; i < adj.size(); ++i) {
        for (int j = 0; j < i; ++j) {
            aux = adj[i][j];
            adj[i][j] = adj[j][i];
            adj[j][i]=aux;
        }
    }
}


int almostShortestPath(std::vector<std::vector<int>> &adj,  int s, int d){
    std::vector<int> distS = dijkstra(adj, s);
    transpose(adj);
    std::vector<int> distD = dijkstra(adj, d);
    transpose(adj);
    for (int i = 0; i < adj.size(); ++i) {
        for (int j = 0; j < adj.size(); ++j) {
            if (adj[i][j]!= INT32_MAX){
                if (distS[i] + adj[i][j] + distD[j] == distS[d]){
                    adj[i][j]=INT32_MAX;
                }
            }
        }
    }
    int res = dijkstra(adj, s)[d];
    if (res == INT32_MAX) res=-1;
    return res;

}