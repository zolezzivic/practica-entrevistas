#include <iostream>
#include <vector>

int numberOfChairs(std::vector<int>& s, std::vector<int>& e);
void quickSort2(std::vector<int>& s, std::vector<int>& e, int left, int right);
int main() {
    std::vector<int> s={3,4,6,5,1};
    std::vector<int> e={10,10,10,10,4};
    std::cout<<numberOfChairs(s,e)<<std::endl;
    return 0;
}

int maxIdx(std::vector<int>& s, int l, int r){
    int idx=-1;
    int max=INT32_MIN;
    for (int i = l; i < r; ++i) {
        if(s[i]>max){
            max=s[i];
            idx=i;
        }
    }
    return idx;
}

void quickSort2(std::vector<int>& s, std::vector<int>& e, int left, int right){
    if(right-left<=1) return;
    int pivot=left+rand()%(right-left);
    int pivotValue=s[pivot];
    std::swap(s[left], s[pivot]);
    std::swap(e[left], e[pivot]);
    int idxMax= maxIdx(s, left+1, right);
    std::swap(s[right-1], s[idxMax]);
    std::swap(e[right-1], e[idxMax]);
    int l=left+1;
    int r=right-1;
    while(r>=l){
        if(s[l]>pivotValue && s[r]<pivotValue) {
            std::swap(s[l], s[r]);
            std::swap(e[l], e[r]);
            l++;
            r--;
        }else{
            if(s[l]<=pivotValue) l++;
            if(s[r]>=pivotValue) r--;
        }
    }
    std::swap(s[left], s[r]);
    std::swap(e[left], e[r]);
    quickSort2(s, e, left, r);
    quickSort2(s, e, r+1, right);

}


int numberOfChairs(std::vector<int>& s, std::vector<int>& e){
    quickSort2(s,e, 0, s.size());
    int idxS=0;
    int idxE=0;
    int minExitTime=e[idxE];
    int maxChairs=0;
    int currentChairs=0;
    while(idxS<s.size() && idxE<s.size()){
        minExitTime=std::max(minExitTime, e[idxE]);
        if(s[idxS]<minExitTime){
            currentChairs++;
            idxS++;
        }else{
            maxChairs=std::max(maxChairs, currentChairs);
            currentChairs--;
            idxE++;
        }
    }
    return std::max(maxChairs,currentChairs);
}