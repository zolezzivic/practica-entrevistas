#include <iostream>
#include <vector>
#include <algorithm>

int bouquets(std::vector<int> & roses, int k, int n);

int main() {
    std::vector<int> roses={2, 3, 4, 9, 5, 6, 10};
    std::cout << bouquets(roses, 2,2) << std::endl;
    return 0;
}

int bouquets(std::vector<int> & roses, int k, int n){
    int min=INT32_MAX;
    int max=INT32_MIN;
    for (int i:roses) {
        if(i>max) max=i;
        if(i<min) min=i;
    }
    int low=min;
    int high=max;
    int mid=(low+high)/2;
    while(high>low+1){
        int amountOfBouquets=0;
        int amountOfBlossomRoses=0;
        int idx=0;
        while(idx<roses.size()){
            if(roses[idx]>mid) {
                amountOfBlossomRoses=0;
                idx++;
            }
            else {
                amountOfBlossomRoses++;
                if(amountOfBlossomRoses>=k) {
                    amountOfBouquets++;
                    amountOfBlossomRoses=0;
                }
                idx++;
            }
        }
        if(amountOfBouquets<n) low=mid;
        if(amountOfBouquets>=n) high=mid;
        mid=high+low;
        mid/=2;
    }
    return high;
}