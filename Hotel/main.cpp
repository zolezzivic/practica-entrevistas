#include <iostream>
#include <vector>
#include <unordered_map>

std::string mostBookedRoom(std::vector<std::string>& r);

int main() {
    std::vector<std::string> v={"+1A", "+3E", "+3E", "+4F", "+1A", "+3E"};
    std::cout << mostBookedRoom(v) << std::endl;
    return 0;
}

std::string mostBookedRoom(std::vector<std::string>& r){
    std::unordered_map<std::string, int> rooms;
    int maxBooked=0;
    std::string maxBookedRoom;
    for(std::string b:r){
        if(b[0]=='+'){
            if(!rooms.count(b)) rooms.insert(std::make_pair(b, 0));
            rooms[b]+=1;
        }
        if(maxBooked==rooms[b] && maxBookedRoom>b){
            maxBooked=rooms[b];
            maxBookedRoom=b;
        }
        else if(maxBooked<rooms[b]){
            maxBooked=rooms[b];
            maxBookedRoom=b;
        }
    }
    return maxBookedRoom;

}