#include <iostream>

std::string maxTime(std::string& t);

int main() {
    std::string t="??:??";
    std::cout << maxTime(t) << std::endl;
    return 0;
}


std::string maxTime(std::string& t){
    std::string res=t;
    if(t[0]=='?'){
        if(t[1]=='?' || t[1]-48<=3) res[0]='2';
        else res[0]='1';
    }
    if(t[1]=='?'){
        if(t[0]!='?' && t[0]-48<2) res[1]='9';
        else res[1]='3';
    }
    if(t[3]=='?') res[3]='5';
    if(t[4]=='?') res[4]='9';
    return res;
}