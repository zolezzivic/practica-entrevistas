#include <iostream>
#include <vector>
#include <queue>
#include <unordered_set>
#include <math.h>

std::vector<std::vector<int>> kClosest(std::vector<std::vector<int>>& points, int k);
float distance(std::vector<int>& p);

int main() {
    std::vector<std::vector<int>> p={{-5,4},{-6,-5},{4,6}};
    std::vector<std::vector<int>> res=kClosest(p, 2);

    for(std::vector<int> v:res){
        printf("%d, %d\n", v[0], v[1]);
    }
    return 0;
}

float distance(std::vector<int>& p){
    int first=std::pow(p[0],2);
    int second=std::pow(p[1],2);
    return sqrt(first+second);

}

std::vector<std::vector<int>> kClosest(std::vector<std::vector<int>>& points, int k){
    std::priority_queue<std::pair<float,int>> closestPoints;
    std::unordered_set<int> setOfClosestPoints;
    closestPoints.push(std::make_pair(distance(points[0]), 0));
    setOfClosestPoints.insert(0);
    for (int i = 1; i < points.size(); ++i) {
        if(closestPoints.size()<k){
            closestPoints.push(std::make_pair(distance(points[i]), i));
            setOfClosestPoints.insert(i);

        }else{
            if(closestPoints.top().first>distance(points[i])){
                setOfClosestPoints.erase(closestPoints.top().second);
                closestPoints.pop();
                closestPoints.push(std::make_pair(distance(points[i]), i));
                setOfClosestPoints.insert(i);

            }
        }
    }
    std::vector<std::vector<int>> res(k, std::vector<int>(2,0));
    int idx=0;
    for (int i: setOfClosestPoints) {
        res[idx]=points[i];
        idx++;
    }
    return res;

}