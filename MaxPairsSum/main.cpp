#include <iostream>
#include <vector>
#include <unordered_map>
#include <algorithm>

class Solution{
public:
    int solution(std::vector<int>& A);
    int solutionBis(std::vector<int>& A);
};

int main() {
    std::vector<int> a={5, 15,20,0,4,16,1};
    Solution S=Solution();
    std::cout << S.solution(a) << std::endl;
    std::cout << S.solutionBis(a) << std::endl;
    return 0;
}


//O(max{maxSum*n, nlogn}) = O(max{2000*n, nlogn})
int Solution::solution(std::vector<int> &A) {
    int maxSum=0;
    for (int i:A) maxSum+=i;
    std::sort(A.begin(),A.end());
    int res=0;
    for (int sum = 1; sum <= maxSum; ++sum) {
        int left=0;
        int right=A.size()-1;
        int amountOfPairsThatSum=0;
        while(right>left){
            if(A[left]+A[right]==sum) {
                amountOfPairsThatSum++;
                left++;
                right--;
            }else if(A[left]+A[right]<sum){
                left++;
            }else right--;
        }
        res=std::max(res, amountOfPairsThatSum);
    }
    return res;
}

int Solution::solutionBis(std::vector<int> &A) {
    int maxSum=0;
    std::unordered_map<int, int> partialSums;
    for (int i:A) maxSum+=i;
    int res=0;
    for (int sum = 1; sum <= maxSum; ++sum) {
        int amountOfPairsThatSum=0;
        partialSums.clear();
        for (int i = 0; i < A.size(); ++i) {
            if(!partialSums.count(abs(A[i]-sum))){
                if(!partialSums.count(A[i])) partialSums.insert(std::make_pair(A[i],0));
                partialSums[A[i]]++;
            }else{
                amountOfPairsThatSum++;
                partialSums[abs(A[i]-sum)]-=1;
                if(partialSums[abs(A[i]-sum)]==0) partialSums.erase(abs(A[i]-sum));
            }
        }
        if(amountOfPairsThatSum>res) res=amountOfPairsThatSum;
    }
    return res;
}