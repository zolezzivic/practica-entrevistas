#include <iostream>
#include <vector>

using namespace std;
int maxSumRectangle(vector<vector<int>> &torus);


int main(int argc, char* argv[]) {
    int nTestCases;
    cin>>nTestCases;
    vector<vector<vector<int>>> t;
    int p;
    int N;
    for(int i = 0; i < nTestCases; i++){
        cin>>N;
        vector<vector<int>> testCase;
        for (int j = 0; j < N; ++j) {
            vector<int> row;
            for (int k = 0; k < N; ++k) {
                cin>>p;
                row.push_back(p);
            }
            testCase.push_back(row);
        }
        t.push_back(testCase);
    }
    for (int i = 0; i < nTestCases; ++i) {
        cout<<maxSumRectangle(t[i])<<endl;
    }
    return 0;
}



int maxSumRectangle(vector<vector<int>> &torus) {
    int mSum=INT32_MIN;
    vector<int> maxSubrect(4, -1);
    int n = torus.size();
    vector<vector<vector<vector<int>>>> memo(n, vector<vector<vector<int>>>(n, vector<vector<int>>(n, vector<int>(n,INT32_MIN))));

    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            memo[i][i][j][j]=torus[i][j];
            if (memo[i][i][j][j]>mSum){
                mSum=memo[i][i][j][j];
                maxSubrect={i,i,j,j};
            }
        }
    }

    int r0, r1, c0, c1;
    for (int i = 0; i < n; ++i) {
        for (int k = 0; k+i < n; ++k) {
            for (int l = 0; l+i < n; ++l) {
                r0 = k;
                r1 = k+i;
                c0 = l;
                c1 = l+i;
                for (int m = 0; m < n; ++m) {
                    int aux = (c1 + m >= n) ? 0 : c1 + m;
                    if (memo[r0][r1][c0][aux] == INT32_MIN){
                        int c1aux=(aux-1<0)? torus.size()-1:aux-1;
                        memo[r0][r1][c0][aux] = memo[r0][r1][aux][aux] + memo[r0][r1][c0][c1aux];
                    }
                    if (memo[r0][r1][c0][aux]>mSum){
                        mSum=memo[r0][r1][c0][aux];
                        maxSubrect={r0,r1,c0,aux};
                    }
                    aux=(c0 - m < 0) ? n - 1 : c0 - m;
                    if(memo[r0][r1][aux][c1]==INT32_MIN){
                        int c0aux=(aux+1 >= torus.size()) ? 0 : aux+1;
                        memo[r0][r1][aux][c1] = memo[r0][r1][aux][aux] + memo[r0][r1][c0aux][c1];
                    }
                    if (memo[r0][r1][aux][c1]>mSum){
                        mSum=memo[r0][r1][aux][c1];
                        maxSubrect={r0,r1,aux,c1};
                    }
                    aux = (r1 + m >= n) ? 0 : r1 + m;
                    if (memo[r0][aux][c0][c1]==INT32_MIN) {
                        int r1aux = (aux-1<0)?torus.size()-1:aux-1;
                        memo[r0][aux][c0][c1] = memo[aux][aux][c0][c1] + memo[r0][r1aux][c0][c1];
                    }
                    if (memo[r0][aux][c0][c1]>mSum){
                        mSum=memo[r0][aux][c0][c1];
                        maxSubrect={r0,aux,c0,c1};
                    }
                    aux=(r0 - m < 0) ? n - 1 : r0 - m;
                    if(memo[aux][r1][c0][c1] ==INT32_MIN) {
                        int r0aux = (aux+1>=torus.size())? 0:aux+1;
                        memo[aux][r1][c0][c1] = memo[aux][aux][c0][c1] + memo[r0aux][r1][c0][c1];
                    }
                    if (memo[aux][r1][c0][c1]>mSum){
                        mSum=memo[aux][r1][c0][c1];
                        maxSubrect={aux,r1,c0,c1};
                    }
                }
            }
        }
    }
    int aux;
    for (int i = 0; i < memo.size(); ++i) {
        for (int j = 0; j < memo[0].size(); ++j) {
            for (int k = 0; k < memo[0][0].size(); ++k) {
                for (int l = 0; l < memo[0][0][0].size(); ++l) {
                    if (memo[i][j][k][l]==INT32_MIN){
                        aux=0;
                        if (i>j){
                            if (k>l){
                                aux += memo[i][torus.size()-1][k][torus.size()-1];
                                aux+=memo[i][torus.size()-1][0][l];
                                aux += memo[0][j][k][torus.size()-1];
                                aux+=memo[0][j][0][l];
                            }else{
                                aux+=memo[i][torus.size()-1][k][l];
                                aux+=memo[0][j][k][l];
                            }
                        }else{
                            if (k>l){
                                aux += memo[i][j][k][torus.size()-1];
                                aux+=memo[i][j][0][l];
                            }else{
                                aux+=memo[i][j][k][l];
                            }
                        }
                        memo[i][j][k][l]=aux;
                        if (aux>mSum){
                            mSum=aux;
                            maxSubrect = {i,j,k,l};
                        }
                    }
                }
            }
        }
    }
    if (test(maxSubrect, torus, memo)) return mSum;
    return INT32_MIN;
    //return maxSubrect;
}
