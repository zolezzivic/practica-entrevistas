#include <iostream>
#include <vector>

using namespace std;
int maxSumRectangle(vector<vector<int>> &torus);
void printMatrix(vector<vector<int>>& m);




int main(int argc, char* argv[]) {

//   int nTestCases;
//   cin>>nTestCases;
//    vector<vector<vector<int>>> t;
//    int p;
//    int N;
//    for(int i = 0; i < nTestCases; i++){
//        cin>>N;
//        vector<vector<int>> testCase;
//       for (int j = 0; j < N; ++j) {
//           vector<int> row;
//           for (int k = 0; k < N; ++k) {
//               cin>>p;
//               row.push_back(p);
//           }
//           testCase.push_back(row);
//       }
//       t.push_back(testCase);
//   }
//    for (int i = 0; i < nTestCases; ++i) {
//        cout<<maxSumRectangle(t[i])<<endl;
//    }


//    {{1,1,0,0,-4},{2,3,- 2,-3,2},{4,1,-1,5,0},{3,-2,1,-3,2}, {-3,2,4,1,-4}};

    vector<vector<int>> t1;
    /*
    for (int i = 2; i < 20; ++i) {
        t1 = vector<vector<int>> (i, vector<int> (i,0));
        cout<<i<<endl;
        for (int j = 0; j < i; ++j) {
            for (int k = 0; k < i; ++k) {
                t1[j][k]= rand() % 29 + (-9);
                cout<<t1[j][k]<<"\t";
            }
            cout<<endl;
        }
        t1[i/2][0]=10;
        t1[i/2-1][0]=10;
        t1[i/2][i-1]=10;
        t1[i/2-1][i-1]=10;

        int maxSubrect = maxSumRectangle(t1);
        if (maxSubrect==INT32_MIN) {
            cout<<"Something is wrong"<<endl;
            return 1;
        }
        cout << maxSubrect<<endl;
    }*/
    t1 = {{1,2},{-3,4}};
    int maxSubrect = maxSumRectangle(t1);
    if (maxSubrect==INT32_MIN) {
        cout<<"Something is wrong"<<endl;
        return 1;
    }
    cout << maxSubrect<<endl;


    return 0;
}
void printMatrix(int r0, int r1, int c0, int c1,vector<vector<int>>& m){
    //cout<<"[";
    if (r0>r1){
        for (int i = r0; i < m.size(); ++i) {
            cout<<"[";
            if (c0>c1){
                for (int j = c0; j < m.size(); ++j) {
                    cout<<m[i][j]<<",";
                }
                for (int j = 0; j <= c1; ++j) {
                    cout<<m[i][j]<<",";
                }
                cout<<"]"<<endl;
            }else{
                for (int j = c0; j <= c1; ++j) {
                    cout<<m[i][j]<<",";
                }
                cout<<"]"<<endl;
            }
        }
        for (int i = 0; i <= r1; ++i) {
            cout<<"[";
            if (c0>c1){
                for (int j = c0; j < m.size(); ++j) {
                    cout<<m[i][j]<<",";
                }
                for (int j = 0; j <= c1; ++j) {
                    cout<<m[i][j]<<",";
                }
                cout<<"]"<<endl;
            }else{
                for (int j = c0; j <= c1; ++j) {
                    cout<<m[i][j]<<",";
                }
                cout<<"]"<<endl;
            }
        }
        cout<<endl;
    }else{
        for (int i = r0; i <= r1; ++i) {
            cout<<"[";
            if (c0>c1){
                for (int j = c0; j < m.size(); ++j) {
                    cout<<m[i][j]<<",";
                }
                for (int j = 0; j <= c1; ++j) {
                    cout<<m[i][j]<<",";
                }
                cout<<"]"<<endl;
            }else{
                for (int j = c0; j <= c1; ++j) {
                    cout<<m[i][j]<<",";
                }
                cout<<"]"<<endl;
            }
        }
       cout<<endl;
    }
}

int rectSum(int r0, int r1, int c0, int c1, vector<vector<int>>& m){
    int res=0;
    if (r0>r1){
        for (int i = r0; i < m.size(); ++i) {
            if (c0>c1){
                for (int j = c0; j < m.size(); ++j) {
                    res+=m[i][j];
                }
                for (int j = 0; j <= c1; ++j) {
                    res+=m[i][j];
                }
            }else{
                for (int j = c0; j <= c1; ++j) {
                    res+=m[i][j];
                }
            }
        }
        for (int i = 0; i <= r1; ++i) {
            if (c0>c1){
                for (int j = c0; j < m.size(); ++j) {
                    res+=m[i][j];
                }
                for (int j = 0; j <= c1; ++j) {
                    res+=m[i][j];
                }
            }else{
                for (int j = c0; j <= c1; ++j) {
                    res+=m[i][j];
                }
            }
        }
    }else{
        for (int i = r0; i <= r1; ++i) {
            if (c0>c1){
                for (int j = c0; j < m.size(); ++j) {
                    res+=m[i][j];
                }
                for (int j = 0; j <= c1; ++j) {
                    res+=m[i][j];
                }
            }else{
                for (int j = c0; j <= c1; ++j) {
                    res+=m[i][j];
                }
            }
        }
    }
    return res;
}

bool test(vector<int>& res,vector<vector<int>> &torus,vector<vector<vector<vector<int>>>>& m){
    int maxSum =INT32_MIN;
    for (int i = 0; i < m.size(); ++i) {
        for (int j = 0; j < m[0].size(); ++j) {
            for (int k = 0; k < m[0][0].size(); ++k) {
                for (int l = 0; l < m[0][0][0].size(); ++l) {
                    if (m[i][j][k][l]!= rectSum(i,j,k,l,torus)){
                        return false;
                    }
                    if(m[i][j][k][l]>maxSum){
                        maxSum=m[i][j][k][l];
                    }
                }
            }
        }
    }

    return rectSum(res[0], res[1],res[2], res[3], torus) == maxSum;
}


int maxSumRectangle(vector<vector<int>> &torus) {
    int mSum=INT32_MIN;
    vector<int> maxSubrect(4, -1);
    int n = torus.size();
    vector<vector<vector<vector<int>>>> memo(n, vector<vector<vector<int>>>(n, vector<vector<int>>(n, vector<int>(n,INT32_MIN))));

    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            memo[i][i][j][j]=torus[i][j];
            if (memo[i][i][j][j]>mSum){
                mSum=memo[i][i][j][j];
                maxSubrect={i,i,j,j};
            }
        }
    }

    int r0, r1, c0, c1;
    for (int i = 0; i < n; ++i) {
        for (int k = 0; k+i < n; ++k) {
            for (int l = 0; l+i < n; ++l) {
                r0 = k;
                r1 = k+i;
                c0 = l;
                c1 = l+i;
                for (int m = 0; m < n; ++m) {
                    int aux = (c1 + m >= n) ? 0 : c1 + m;
                    if (memo[r0][r1][c0][aux] == INT32_MIN){
                        int c1aux=(aux-1<0)? torus.size()-1:aux-1;
                        memo[r0][r1][c0][aux] = memo[r0][r1][aux][aux] + memo[r0][r1][c0][c1aux];
                    }
                    if (memo[r0][r1][c0][aux]>mSum){
                        mSum=memo[r0][r1][c0][aux];
                        maxSubrect={r0,r1,c0,aux};
                    }
                    aux=(c0 - m < 0) ? n - 1 : c0 - m;
                    if(memo[r0][r1][aux][c1]==INT32_MIN){
                        int c0aux=(aux+1 >= torus.size()) ? 0 : aux+1;
                        memo[r0][r1][aux][c1] = memo[r0][r1][aux][aux] + memo[r0][r1][c0aux][c1];
                    }
                    if (memo[r0][r1][aux][c1]>mSum){
                        mSum=memo[r0][r1][aux][c1];
                        maxSubrect={r0,r1,aux,c1};
                    }
                    aux = (r1 + m >= n) ? 0 : r1 + m;
                    if (memo[r0][aux][c0][c1]==INT32_MIN) {
                        int r1aux = (aux-1<0)?torus.size()-1:aux-1;
                        memo[r0][aux][c0][c1] = memo[aux][aux][c0][c1] + memo[r0][r1aux][c0][c1];
                    }
                    if (memo[r0][aux][c0][c1]>mSum){
                        mSum=memo[r0][aux][c0][c1];
                        maxSubrect={r0,aux,c0,c1};
                    }
                    aux=(r0 - m < 0) ? n - 1 : r0 - m;
                    if(memo[aux][r1][c0][c1] ==INT32_MIN) {
                        int r0aux = (aux+1>=torus.size())? 0:aux+1;
                        memo[aux][r1][c0][c1] = memo[aux][aux][c0][c1] + memo[r0aux][r1][c0][c1];
                    }
                    if (memo[aux][r1][c0][c1]>mSum){
                        mSum=memo[aux][r1][c0][c1];
                        maxSubrect={aux,r1,c0,c1};
                    }
                }
            }
        }
    }
    int aux;
    for (int i = 0; i < memo.size(); ++i) {
        for (int j = 0; j < memo[0].size(); ++j) {
            for (int k = 0; k < memo[0][0].size(); ++k) {
                for (int l = 0; l < memo[0][0][0].size(); ++l) {
                    if (memo[i][j][k][l]==INT32_MIN){
                        aux=0;
                        if (i>j){
                            if (k>l){
                                aux += memo[i][torus.size()-1][k][torus.size()-1];
                                aux+=memo[i][torus.size()-1][0][l];
                                aux += memo[0][j][k][torus.size()-1];
                                aux+=memo[0][j][0][l];
                            }else{
                                aux+=memo[i][torus.size()-1][k][l];
                                aux+=memo[0][j][k][l];
                            }
                        }else{
                            if (k>l){
                                aux += memo[i][j][k][torus.size()-1];
                                aux+=memo[i][j][0][l];
                            }else{
                                aux+=memo[i][j][k][l];
                            }
                        }
                        memo[i][j][k][l]=aux;
                        if (aux>mSum){
                            mSum=aux;
                            maxSubrect = {i,j,k,l};
                        }
                    }
                }
            }
        }
    }
    if (test(maxSubrect, torus, memo)) return mSum;
    return INT32_MIN;
    //return maxSubrect;
}



/*
  int r0, r1, c0, c1;
    for (int i = 0; i < n; ++i) {       //start from every number
        for (int j = 0; j < n; ++j) {
            for (int k = 0; k +i< n; ++k) {   //increase row
                for (int l = 0; l+j< n; ++l) {   //increase col
                    r0 = k;
                    r1 = k + i;
                    c0 = l;
                    c1 = l + j;
                    memo[r0][r1][c0][c1] = memo[r0][max(0, k + i - 1)][c0][max(0, l + j - 1)] + memo[r1][r1][c1][c1];
                    if (memo[r0][r1][c0][c1] > mSum) {
                        mSum = memo[r0][r1][c0][c1];
                        maxSubrect = {r0, r1, c0, c1};
                    }
                }
            }
        }
    }
 */