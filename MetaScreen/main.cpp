#include <iostream>
#include <vector>
#include <algorithm>
#include <math.h>

using namespace std;


vector<int> sumOne(vector<int>& num){
    int idx=num.size()-1;
    vector<int> res;
    bool carry=false;

    while(idx>=0 && num[idx]==9){
        res.push_back(0);
        if(idx==0)carry=true;
        idx--;
    }

    if(idx>=0) {
        res.push_back(num[idx]+1);
        idx--;
    }

    while(idx>=0) {
        res.push_back(num[idx]);
        idx--;
    }

    if(carry) res.push_back(1);
    reverse(res.begin(), res.end());

    return res;
}

int distanceBetweenIntervals(vector<int>& v1, vector<int>& v2){
    int distanceFirst= v1[1]-v1[0];
    int distanceSecond= v2[1]-v2[0];
    return abs(distanceFirst-distanceSecond);
}


vector<vector<int>> findMinIntervalsThatCover(vector<vector<int>>& intervals, vector<int> target){
    sort(intervals.begin(), intervals.end());
    vector<vector<int>> res;
    int t0=target[0];
    int t1=target[1];

    vector<int> currentBestInterval={INT32_MIN, INT32_MIN};

    for (int i = 0; i < intervals.size(); ++i) {
        if(intervals[i][0] <= t0){
            if(intervals[i][1]> currentBestInterval[1]){
                currentBestInterval=intervals[i];
                if(currentBestInterval[1]>=t1){
                    res.push_back(currentBestInterval);
                    break;
                }
            }
        }else{
            res.push_back(currentBestInterval);
            t0=currentBestInterval[1];
            if(intervals[i][0]>t0) return {};
            i--;
        }
    }

    return res;
}






int main() {

    vector<vector<int>> num={{0,15},{2,20},{20,30},{4,10},{1,2},{15,30}};

    vector<vector<int>> res= findMinIntervalsThatCover(num, {2,30});

    std::cout << "Hello, World!" << std::endl;
    return 0;
}
