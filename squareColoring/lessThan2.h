#ifndef SQUARECOLORING_LESSTHAN2_H
#define SQUARECOLORING_LESSTHAN2_H
#include <vector>
#include <tuple>

int differentColoringsPD(int);
int differentColoringsBFAux(int, std::vector<int>&);
int differentColoringsBF(int);
bool validColoring (std::vector<int>&);
bool testDiffColorings();


#endif //SQUARECOLORING_LESSTHAN2_H
