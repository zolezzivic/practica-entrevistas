#include <cstdio>
#include "lessThan2.h"


int main() {
    int resFB = differentColoringsBF(5);
    int resPD = differentColoringsPD(5);
    printf("The FB result is: %d\n", resFB);
    printf("The PD result is: %d\n", resPD);
    if (testDiffColorings()){
        printf("All tests have been passed correctly\n");
    }else{
        printf("The tests have failed\n");
    }
    return 0;
}

/*

namespace B {
  #include "B.cpp"
}
namespace C {
  #include "C.cpp"
}

int main ()
{
  A::getNumber();
  B::getNumber();
  C::getNumber();
}
 */