#include "lessThan2.h"

//To determine the number of ways we can color n squares without having more than 2 consecutives squares painted with the
//same color, I will use Dynamic Programming.
//The idea will be to calculate the amount of ways we can paint i squares, where 0<i<=n, because, knowing how many ways there
//are to paint i-1 squares, we can determine the amount of ways we have for i squares. If we can paint i-1 squares in x
//different ways, we can paint i squares in x plus the number of colorings that don't have the last two squares painted
//with the same color in the i-1 case. This is because we only have one way to extend the solutions where the last two
//squares share color (we can say this only if we only have two colors), and two ways in the other case.
//I will use a vector (memo) of n positions, where m[k] is a tuple that indicates the result for k squares and the amount
//of ways that have the last two squares painted with different colors.
//Notice that the amount of ways that have the last two squares painted differently for i squares is equivalent to
//the number of partial solutions that have the last two squares painted with the same color for i-1squares.



int differentColoringsPD(int n){
   std::vector<std::tuple<int,int>> memo(n);
   memo[0] = (std::tuple<int,int>(2,0));
   int totalWays;
    int diffcolorEnding;
    for (int i = 1; i < n; ++i) {
        totalWays = std::get<0>(memo[i-1]);
        diffcolorEnding = totalWays - std::get<1>(memo[i-1]);
        memo[i] = std::tuple<int,int> (totalWays + diffcolorEnding, diffcolorEnding);
    }
    return std::get<0>(memo[n-1]);
}


//To check if the Dynamic Programming algorithm is correct, I will use a Brute Force algorithm.
//The idea will be to generate partial solutions where we have colored i squares and extend them with the coloring of
//a new square. If we have two colors, there are only two possibilities, so we can extend every partial solutions in two
//different ways.
//When we have colored n squares, we are ready to determine if our solution is valid or not, we just need to check that
//there are not three consecutive squares that share color.



bool validColoring(std::vector<int>& coloring){
    if (coloring.size()<3) return true;
    for (int i = 0; i < coloring.size()-2; ++i) {
        if (coloring[i] == coloring[i+1] && coloring[i]==coloring[i+2]) return false;
    }
    return true;
}

int differentColoringsBFAux(int n, std::vector<int>& coloring){
    if (n==0){
        if (validColoring(coloring)){
                return 1;
            }else{
                return 0;
            }
        }
    int res=0;
    coloring.push_back(0);
    res += differentColoringsBFAux(n-1, coloring);
    coloring[coloring.size()-1]=1;
    res+=differentColoringsBFAux(n-1, coloring);
    coloring.pop_back();
    return res;
}

int differentColoringsBF(int n){
    std::vector<int> a;
    return differentColoringsBFAux(n, a);
}


bool testDiffColorings() {
    for (int i = 1; i < 30; ++i) {
        if (differentColoringsBF(i) != differentColoringsPD(i)) {
            return false;
        }
    }
    return true;
}

/*
int numberOfWaysBT=0;
void differentColoringsBFAux(int n, std::vector<int>& lastColors){
    if (n==0){
        if (lastColors[0] != lastColors[1] || lastColors[1]!=lastColors[3]){
        if (validColoring(lastColors)){
            numberOfWaysBT ++;
            return;
        }else{
            return;
        }
    }
    if (lastColors[0] != lastColors[1] || lastColors[1]!=lastColors[3]){
        int color0 =lastColors[0];
        lastColors[0] = lastColors[1];
        lastColors[1] = lastColors[2];
        lastColors[2]= 0;
        differentColoringsBFAux(n-1, lastColors);
        lastColors[2]=1;
        differentColoringsBFAux(n-1, lastColors);
        lastColors [2]=lastColors[1];
        lastColors [1]=lastColors[0];
        lastColors[0]=color0;

    }else{
        return;
    }
}

int differentColoringsBF(int n){
    numberOfWaysBT = 0;
    std::vector<int> lastColors= {-1,-1,-2};
    differentColoringsBFAux(n, lastColors);
    return numberOfWaysBT;
}


*/