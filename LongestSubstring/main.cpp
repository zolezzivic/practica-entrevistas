#include <iostream>
#include <vector>

class Solution{
public:
    std::string solution(std::string& S);
};

int main() {
    Solution S=Solution();
    std::string a="abcdeffghij";
    std::cout << S.solution(a) << std::endl;
    return 0;
}


std::string Solution::solution(std::string &S) {
    int longestSubstring=0;
    char letterOfBeginningAndEnd;
    std::vector<std::pair<int,int>> appearences('z'-'a'+1, std::make_pair(-1,-1));
    for (int i = 0; i < S.size(); ++i) {
        if(appearences[S[i]-'a'].first==-1) appearences[S[i]-'a']=std::make_pair(i,i);
        else appearences[S[i]-'a'].second=i;
        if(appearences[S[i]-'a'].second-appearences[S[i]-'a'].first+1>longestSubstring){
            longestSubstring=appearences[S[i]-'a'].second-appearences[S[i]-'a'].first+1;
            letterOfBeginningAndEnd=S[i];
        }
    }
    int beg=appearences[letterOfBeginningAndEnd-'a'].first;
    int end=appearences[letterOfBeginningAndEnd-'a'].second;
    std::string res;
    for (int j = beg; j <= end; ++j) {
        res.push_back(S[j]);
    }
    return res;
}