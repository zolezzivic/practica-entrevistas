#ifndef MAXDISTANCE_MAXD_HPP
#define MAXDISTANCE_MAXD_HPP

#include <iostream>
#include <utility>
#include <vector>
#include <queue>

struct Nodo{
    std::string value;
    int level;
    Nodo* der;
    Nodo* izq;
    Nodo(std::string val, int level): value(std::move(val)), der(nullptr), izq(nullptr), level(level) {}
    Nodo(int level): value(""), der(nullptr), izq(nullptr), level(level){}

};

class Trie{
public:
    Trie(): _head(new Nodo(0)){}
    void insertar(std::string s);
    Nodo* getHead();
private:
    Nodo* _head;
};

int maxDistance(std::vector<std::string> &S);
#endif //MAXDISTANCE_MAXD_HPP
