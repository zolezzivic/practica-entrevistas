#include "maxD.hpp"


Nodo * Trie::getHead() {
    return _head;
}


void Trie::insertar(std::string s) {
    Nodo* currentNode= getHead();
    for(char c:s){
        if(c=='0'){
            if(currentNode->izq== nullptr){
                currentNode->izq=new Nodo(currentNode->level+1);
            }
            currentNode= currentNode->izq;
        }else{
            if(currentNode->der== nullptr){
                currentNode->der=new Nodo(currentNode->level+1);
            }
            currentNode= currentNode->der;
        }
    }
    currentNode->value=s;
}

int maxDistance(std::vector<std::string> &S){
    Trie* trie= new Trie();
    for(std::string s:S) trie->insertar(s);
    std::queue<Nodo*> nodes;
    nodes.push(trie->getHead());
    int maxLength=0;
    while(! nodes.empty()){
        Nodo* current= nodes.front();
        nodes.pop();
        if(current->izq!= nullptr){
            nodes.push(current->izq);
        }
        if(current->der!= nullptr){
            nodes.push(current->der);
        }
    }
    return maxLength;
}