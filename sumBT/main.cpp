#include <iostream>
#include <queue>
#include <unordered_map>

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

int maxLevelSum(TreeNode* root);

int main() {
    TreeNode* root= new TreeNode(-100);
    root->left= new TreeNode(-200);
    root->right= new TreeNode(-300);
    root->left->left=new TreeNode(-20);
    root->left->right=new TreeNode(-5);
    root->right->left=  new TreeNode(-10);



    std::cout << maxLevelSum(root) << std::endl;
    return 0;
}

int maxLevelSum(TreeNode* root){
    std::queue<TreeNode*> visitedNodes;
    visitedNodes.push(root);
    std::unordered_map<int,int> levelSum;
    std::unordered_map<TreeNode*, int> nodesLevel;
    nodesLevel.insert(std::make_pair(root, 1));
    levelSum.insert(std::make_pair(1,root->val));

    while(!visitedNodes.empty()){
        TreeNode* currentFather= visitedNodes.front();
        visitedNodes.pop();
        int currentLevel=nodesLevel[currentFather]+1;
        if(currentFather->left!= nullptr) {
            nodesLevel.insert(std::make_pair(currentFather->left, currentLevel));
            visitedNodes.push(currentFather->left);

            if(!levelSum.count(currentLevel)) levelSum.insert(std::make_pair(currentLevel,0));\

            levelSum[currentLevel]+=currentFather->left->val;
        }
        if(currentFather->right!= nullptr) {
            nodesLevel.insert(std::make_pair(currentFather->right, currentLevel));
            visitedNodes.push(currentFather->right);
            if(!levelSum.count(currentLevel)) levelSum.insert(std::make_pair(currentLevel,0));\

            levelSum[currentLevel]+=currentFather->right->val;
        }
    }
    int max=INT32_MIN;
    int res=-1;
    for(auto keyValue: levelSum){
        if(keyValue.second>=max){
            if(keyValue.second==max) res=std::min(keyValue.first, res);
            else res=keyValue.first;
            max=keyValue.second;
        }
    }
    return res;
}