#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <stack>
#include <math.h>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>

using namespace std;

struct distanceAndPoint{
    float distance;
    int pointIdx;
    distanceAndPoint(): distance(-1), pointIdx(-1){}
    distanceAndPoint(float d, int p): distance(d), pointIdx(p) {}
    bool operator < (const struct distanceAndPoint& other) const{
        return (distance<other.distance);
    }
};

template<class T> void printVector(vector<T>& v){
    for (T elem:v) cout<<elem<<",";
    printf("\n");
}


struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};


struct coordinate{
    int x;
    int y;
    coordinate(): x(0), y(0){};
    coordinate(int x, int y): x(x), y(y){};
    bool operator < (const struct coordinate other) const {
        if(x<other.x) return true;
        if(x==other.x) return y<other.y;
        return false;
    }
    bool operator == (const struct coordinate other) const{
        if(x!=other.x) return false;
        return y == other.y;
    }



};
//

struct coord{
    int rows;
    int cols;
    coord(): rows(-1), cols(-1){};
    coord(int a, int b): rows(a), cols(b){};
};

int uniquePaths(int m, int n) {
    stack<coord> paths;
    paths.push(coord(0, 0));
    int res = 0;

    coord position;
    while (!paths.empty()) {
        position = paths.top();
        paths.pop();
        if (position.rows == m - 1 && position.cols == n - 1) res++;
        else {
            if (position.rows < m - 1) paths.push(coord(position.rows + 1, position.cols));
            if (position.cols < n - 1) paths.push(coord(position.rows, position.cols + 1));
        }
    }
    return res;
}





int main() {
    //TreeNode* tree= new TreeNode(5, new TreeNode(3, new TreeNode(2, new TreeNode(1), nullptr),
     //       new TreeNode(4)), new TreeNode (6, new TreeNode(8,new TreeNode(7), nullptr),new TreeNode(8)));
    //TreeNode* tree= new TreeNode(2, nullptr, new TreeNode(5, nullptr,new TreeNode(7)));

    //ListNode* l= new ListNode(1, new ListNode(2,new ListNode(3,new ListNode(4,nullptr))));
    //swapPairs(l);

    cout<<uniquePaths(2,2)<<endl;

    return 0;
}
