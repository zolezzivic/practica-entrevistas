#include <iostream>
#include <unordered_set>
#include <unordered_map>

int numberOfWaysOfSplitting(std::string& s);
int numberOfWaysOfSplittingBis(std::string& s);

int main() {
    std::string s="aacaba";
    std::cout << numberOfWaysOfSplittingBis(s) << std::endl;
    return 0;
}

//O(n^2)
int numberOfWaysOfSplitting(std::string& s){
    std::unordered_set<char> S1;
    std::unordered_set<char> S2;
    int res=0;
    for(int i=0; i<s.size();i++){
        S1.insert(s[i]);
        S2.clear();
        for (int j = i+1; j < s.size(); ++j) {

            if (!S2.count(s[j])) S2.insert(s[j]);
        }
        if(S1.size()==S2.size()) res++;
    }
    return res;
}

//O(n)
int numberOfWaysOfSplittingBis(std::string& s){
    if(s.size()<2) return 0;
    int res=0;
    std::unordered_map<char, int> S1;
    std::unordered_map<char, int> S2;
    S1.insert(std::make_pair(s[0],1));
    for (int i = 1; i < s.size(); ++i) {
        if(!S2.count(s[i])) S2.insert(std::make_pair(s[i],0));
        S2[s[i]]+=1;
    }
    if(S1.size()==S2.size()) res++;
    for (int j = 1; j < s.size(); ++j) {
        if(!S1.count(s[j])) S1.insert(std::make_pair(s[j],0));
        S1[s[j]]+=1;
        if(S2[s[j]]==1) S2.erase(s[j]);
        else S2[s[j]]-=1;
        if(S1.size()==S2.size()) res++;
    }
    return res;
}