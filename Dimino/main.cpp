#include <iostream>
#include <vector>

int minDominoRotations(std::vector<int>& tops, std::vector<int>& bottoms);

int main() {
    std::vector<int> t={2,1,2,4,2,2};
    std::vector<int> b={5,2,6,2,3,2};

    std::cout << minDominoRotations(t,b) << std::endl;
    return 0;
}

int minDominoRotations(std::vector<int>& tops, std::vector<int>& bottoms){
    int res=INT32_MAX;
    int amountTop=0;
    int amountBottom=0;
    int amountBoth=0;
    for (int i = 1; i <= 6; ++i) {
        amountTop=0;
        amountBottom=0;
        amountBoth=0;
        for (int j = 0; j < tops.size(); ++j) {
            if(tops[j]==i && bottoms[j]==i) amountBoth++;
            else{
                if(tops[j]==i) amountTop++;
                else if(bottoms[j]==i) amountBottom++;
            }
        }
        if(amountBoth+amountBottom+amountTop<tops.size()) continue;

        if(amountTop>amountBottom){
            res=std::min(res, (std::max(0,(int)tops.size()-amountBoth-amountTop)));
        }else{
            res=std::min(res, (std::max(0,(int)tops.size()-amountBoth-amountBottom)));
        }
    }
    if(res==INT32_MAX) res=-1;
    return res;
}