#include <iostream>
#include<string>
#include <stack>
#include <algorithm>

using namespace std;


void removeKDuplicatesFromStack(stack<char>& s, int k){
    if(s.size()<k) return;
    stack<char> aux;
    char duplicateCandidate= s.top();
    int amountOfDuplicates=0;

    while(!s.empty()){
        if(s.top()!= duplicateCandidate){
            break;
        }
        else{
            amountOfDuplicates++;
            aux.push(s.top());
            s.pop();
        }
    }
    if(amountOfDuplicates>=k){
        for (int i = 0; i < k; ++i) {
            aux.pop();
        }
    }
    while(!aux.empty()){
        s.push(aux.top());
        aux.pop();
    }

}


//string removeDuplicates(string& s, int k) {
//    stack<pair<char,int>> elements;
//    pair<char, int> topElement;
//
//    for (int i = 0; i < s.size(); ++i) {
//        if(!elements.empty() && elements.top().first==s[i]){
//            topElement= elements.top();
//            topElement.second+=1;
//            elements.pop();
//            if(topElement.second<k)  elements.push(topElement);
//        }else{
//            topElement=make_pair(s[i],1);
//            elements.push(topElement);
//        }
//    }
//
//    string res;
//    while(!elements.empty()){
//        for (int i = 0; i < elements.top().second; ++i) {
//            res.push_back(elements.top().first);
//        }
//        elements.pop();
//    }
//    reverse(res.begin(),res.end());
//    return res;
//
//}


struct letterAndAdy{
    char letter;
    int cantAdy;
    letterAndAdy(char l, int c): letter(l), cantAdy(c){};
};


string removeDuplicates(string& s, int k) {
    stack<letterAndAdy*> elements;
    letterAndAdy* topElement;

    for (int i = 0; i < s.size(); ++i) {
        if(!elements.empty() && elements.top()->letter==s[i]){
            topElement= elements.top();
            topElement->cantAdy+=1;
            if(topElement->cantAdy==k)  elements.pop();
        }else{
            topElement= new letterAndAdy(s[i],1);
            elements.push(topElement);
        }
    }

    string res;
    while(!elements.empty()){
        for (int i = 0; i < elements.top()->cantAdy; ++i) {
            res.push_back(elements.top()->letter);
        }
        elements.pop();
    }
    reverse(res.begin(),res.end());
    return res;

}

int main() {
    string s="aacbb";
    std::cout << removeDuplicates(s, 2) << std::endl;
    return 0;
}
