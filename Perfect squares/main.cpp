#include <iostream>
#include <math.h>
#include <vector>

int numSquares(int n);
int numSquaresAux(int n);
std::vector<int> memo;


int main() {
    std::cout << numSquares(98) << std::endl;
    return 0;
}

int numSquares(int n){
    memo=std::vector<int>(n+1,INT32_MAX);
    return numSquaresAux(n);
}

int numSquaresAux(int n){
    if((int)(sqrt(n))*(int)(sqrt(n))==n) return 1;  //If it is a perfect square, then it is the sum of itself.
    if(memo[n]==INT32_MAX){
        int logn= sqrt(n);
        int minSquares=INT32_MAX;
        for (int i = 1; i <= logn; ++i) {
            if(n-pow(i,2)>=0){
                int cur=numSquaresAux(n-pow(i,2));
                minSquares=std::min(minSquares, 1+cur);
            }
        }
        memo[n]=minSquares;
    }
    return memo[n];
}