#ifndef PROBLEMAS_ENTREVISTAS_TESTS_H
#define PROBLEMAS_ENTREVISTAS_TESTS_H
#include "ventana.h"

bool ventana_valida(const vector<vector<int>>& arreglo, int e1, int e2);
tuple<int, int> ventana_fb(const vector<vector<int>>& arreglo);
bool test_ventana();
#endif //PROBLEMAS_ENTREVISTAS_TESTS_H
