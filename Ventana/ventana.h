#ifndef VENTANA_H
#define VENTANA_H

#include <iostream>
#include <vector>
#include <tuple>
#include <queue>

using namespace std;
struct elem_and_idx{
    int val;
    int idx;
    elem_and_idx(int valor, int indice) : val(valor), idx(indice){}
    bool operator<(const struct elem_and_idx& other) const{
        return val < other.val;
    }
};


int idx_min_aparicion(priority_queue<elem_and_idx>& mins);
tuple<int, int> ventanaQueContiene(const vector<vector<int>>& arreglo);


#endif //VENTANA_H