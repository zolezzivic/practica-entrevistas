#include "tests.h"
#include <algorithm>


//To test the algorithm, I will generate random instances of the problem and compare the solution provided by the
//Brute Force algorithm to the one provided by my algorithm.
//The Brute Force version analyses every possible interval and updates the best interval found as far if there is a smaller
// valid interval, i.e. it contains every word. At the end of the execution, the smallest interval is returned.

bool ventana_valida(const vector<vector<int>>& arreglo, int e1, int e2){
    int left= min(e1, e2);
    int right = max(e1, e2);
    bool palabra_ok=false;
    for (int i = 0; i < arreglo.size(); ++i) {
        for (int j = 0; j < arreglo[i].size(); ++j) {
            if (arreglo[i][j] >= left && arreglo[i][j] <= right) {
                palabra_ok = true;
                break;
            }
        }
        if (!palabra_ok) {
            return false;
        }
        palabra_ok=false;
    }
    return true;
}

tuple<int, int> ventana_fb(const vector<vector<int>>& arreglo){
    int right=INT32_MAX;
    int left=0;
    int elem1;
    int elem2;

    if (arreglo.size()==1){
        tuple<int,int> res(arreglo[0][0], arreglo[0][0]);
        return res;
    }
    for (int subarr1 = 0; subarr1 < arreglo.size(); ++subarr1) {
        for (int i = 0; i < arreglo[subarr1].size(); ++i) {
            elem1=arreglo[subarr1][i];
            for (int subarr2 = 0; subarr1!=subarr2 && subarr2< arreglo.size(); ++subarr2) {
                for (int j = 0; j < arreglo[subarr2].size(); ++j) {
                    elem2=arreglo[subarr2][j];
                    if (ventana_valida(arreglo,elem1, elem2)){
                        if (abs(elem2-elem1) < abs(right - left)){
                            right= max(elem2, elem1);
                            left= min(elem2, elem1);
                        }
                    }
                }
            }
        }
    }
    tuple<int, int> res(left, right);
    return res;
}

vector<vector<int>> generar_instancia(int m){
    vector<vector<int>> res;
    int tam;
    for (int i=0; i<m;i++){
        tam=rand() % 10 +1;
        vector<int> palabra(tam,0);
        for (int j = 0; j < tam; ++j) {
            palabra[j]=rand() % 50;
        }
        sort(palabra.begin(), palabra.end());
        res.push_back(palabra);
    }
    return res;

}

bool intervalo_mismo_largo(tuple<int,int> a, tuple<int,int> b){
    int dist1 = get<1>(a)-get<0>(a);
    int dist2 =get<1>(b)-get<0>(b);
    return dist1==dist2;
}

bool test_ventana(){
    int m=50;
    int tam;
    for (int i = 1; i < m; ++i) {
        tam =rand()%40 +1;
        for (int j = 1; j < tam; ++j) {
            vector<vector<int>> inst=generar_instancia(i);
            tuple<int,int> res_fb = ventana_fb(inst);
            tuple<int, int> res = ventanaQueContiene(inst);
            if (!intervalo_mismo_largo(res_fb,res)){
                cout << "The test has failed"<<endl;
                cout<< "Expected: (" << get<0>(res_fb) <<", " <<get<1>(res_fb) <<")\t" << "Obtained: ("<< get<0>(res) <<", " <<get<1>(res) <<")"<<endl;
                return false;
            }
        }
    }
    cout<<"All tests have passed correctly"<<endl;
    return true;

}