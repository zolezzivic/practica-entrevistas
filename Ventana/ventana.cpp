#include "ventana.h"


//Our aim is to find the smallest interval that contains all of the words stored in the array.
//Knowing that the first element of each subarray is the first occurrence of each word, we can
//build a first interval by taking the minimum and the maximum element of all firsts elements as its boundaries.
//Notice that if we increase the index of the minimum element by one and repeat the process we followed to
// build the interval, we will get a new one that contains all of the words and turns out to be smaller than the previous one.
//To implement this, we will use a vector to store the indeces of the elements we are analysing on each iteration(we will
// call them the current elements). This stucture will be initialized with zeros.
//Then, our algorithm will choose the minimum of the current elements and will try to increase the index that corresponds
//to the minimum occurrence. If it is possible, i.e. there are more elements in the subarray, after recalculating the
// minimum and the maximum element of the current ones, a new interval will be built and compared to the old one (which is
// the best interval that has been found as far). If the new one is smaller than the old one, the old one will be
// replaced. Otherwise, the algorithm will finish because not being able to increase the minimum element's index means
// that we can't choose a bigger left boundary that still contains that word.

//This algorithm's complexity is equivalent to visit each element of each subarray once and calculating on every iteration
// the minimum of a set of m elements (where m represents the size of the array), which can be run in O(log m) if we use a
// minheap. Updating the maximum element is a constant operation: we just need to compare the current value to the new
// element that is going to be analysed on the next iteration, since the other values remain constant.
//Then, the algorithm is O(n log m), where n represents the total number of occurrences of every word.



int idx_min_aparicion(priority_queue<elem_and_idx>& mins){
    elem_and_idx minimos = mins.top();
    return minimos.idx;
}



tuple<int, int> ventanaQueContiene(const vector<vector<int>>& arreglo){
    vector<int> indices(arreglo.size(), 0);//Array of indices used to analyse one element per subarray
    int subarray_max=-1;                        //Subarray that corresponds to the minimum of the elements pointed by the indices
    int right=INT32_MIN;
    //I will use a priority queu to store the elements pointed by the indices multiplied by -1 to use the queu as a minheap
    priority_queue<elem_and_idx> minimos;
    for (int i = 0; i < arreglo.size(); ++i) {
        minimos.push(elem_and_idx(-1*arreglo[i][0], i));
        if (arreglo[i][0] > right){
            right = arreglo[i][0];
            subarray_max=i;
        }
    }

    int subarray_min=idx_min_aparicion(minimos);  //Subarray which index point to the minimun element of the pointed elements
    int left= arreglo[subarray_min][0];

    int left_aux;
    int right_aux;

    while(indices[subarray_min] < arreglo[subarray_min].size()){
        left_aux=arreglo[subarray_min][indices[subarray_min]];
        right_aux=arreglo[subarray_max][indices[subarray_max]];
        if (right_aux - left_aux < right - left){
            right=right_aux;
            left=left_aux;
        }
        if (indices[subarray_min]+1 >= arreglo[subarray_min].size()){
            break;
        }
        indices[subarray_min]++;
        minimos.pop();
        minimos.push(elem_and_idx(-1*arreglo[subarray_min][indices[subarray_min]], subarray_min));
        if (arreglo[subarray_min][indices[subarray_min]] > arreglo[subarray_max][indices[subarray_max]]){
            subarray_max= subarray_min;
        }
        subarray_min= idx_min_aparicion(minimos);
    }
    tuple<int,int> res(left, right);
    return res;
}


