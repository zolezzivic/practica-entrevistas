#include <iostream>
#include <vector>

int minAmplitud(std::vector<int> &);

int main() {
    std::vector<int> v={10, 1, 3, 4, 10};
    std::cout << minAmplitud(v) << std::endl;
    return 0;
}

int minAmplitud(std::vector<int> &nums) {
    if (nums.size() <= 4) return 0;
    int min = INT32_MAX;
    int minIdx = -1;
    int max = INT32_MIN;
    int maxIdx = -1;
    for (int i = 0; i < nums.size(); ++i) {
        if (nums[i] < min) {
            min = nums[i];
            minIdx = i;
        }
        if (nums[i] > max) {
            max = nums[i];
            maxIdx = i;
        }
    }
    int min1 = INT32_MAX;
    int min1Idx = -1;
    int max1 = INT32_MIN;
    int max1Idx = -1;
    for (int i = 0; i < nums.size(); ++i) {
        if (i == maxIdx || i == minIdx) continue;
        if (nums[i] <= min1) {
            min1 = nums[i];
            min1Idx = i;
        }
        if (nums[i] >= max1) {
            max1 = nums[i];
            max1Idx = i;
        }
    }

    int min2 = INT32_MAX;
    int min2Idx = -1;
    int max2 = INT32_MIN;
    int max2Idx = -1;
    for (int i = 0; i < nums.size(); ++i) {
        if (i == maxIdx || i == minIdx || i == max1Idx || i == min1Idx) continue;
        if (nums[i] <= min2) {
            min2 = nums[i];
            min2Idx = i;
        }
        if (nums[i] >= max2) {
            max2 = nums[i];
            max2Idx = i;
        }
    }

    int res = INT32_MAX;
    int minAux = INT32_MAX;
    int maxAux = INT32_MIN;

    for (int k = 0; k < nums.size(); ++k) {
        if (k != minIdx && k != min1Idx && k != min2Idx) {
            if (minAux > nums[k]) minAux = nums[k];
            if (maxAux < nums[k]) maxAux = nums[k];
        }
    }
    res=maxAux-minAux;
    minAux = INT32_MAX;
    maxAux = INT32_MIN;

    for (int k = 0; k < nums.size(); ++k) {
        if (k != maxIdx && k != max1Idx && k != max2Idx) {
            if (minAux > nums[k]) minAux = nums[k];
            if (maxAux < nums[k]) maxAux = nums[k];
        }
    }
    res=std::min(res, maxAux-minAux);
    minAux = INT32_MAX;
    maxAux = INT32_MIN;

    for (int k = 0; k < nums.size(); ++k) {
        if (k != minIdx && k != maxIdx && k != max1Idx) {
            if (minAux > nums[k]) minAux = nums[k];
            if (maxAux < nums[k]) maxAux = nums[k];
        }
    }
    res=std::min(res, maxAux-minAux);
    minAux = INT32_MAX;
    maxAux = INT32_MIN;
    for (int k = 0; k < nums.size(); ++k) {
        if (k != minIdx && k != maxIdx && k != min1Idx) {
            if (minAux > nums[k]) minAux = nums[k];
            if (maxAux < nums[k]) maxAux = nums[k];
        }
    }
    res=std::min(res, maxAux-minAux);
    return res;
}