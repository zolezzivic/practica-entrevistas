#include <iostream>

std::string dogsAndCats(int n, int d, int c, int m, std::string s);

int main() {
    int numberOfTest;
    scanf("%d",&numberOfTest);
    int N, D, C, M;
    for (int i = 0; i < numberOfTest; ++i) {
        scanf("%d", &N);
        scanf("%d", &D);
        scanf("%d", &C);
        scanf("%d", &M);
        std::string S;
        char c;
        scanf("%c", &c);
        for (int j = 0; j < N; ++j) {
            scanf("%c", &c);
            S.push_back(c);
        }
        std::cout<<"Case #"<<i+1<<": "<<dogsAndCats(N,D,C,M, S)<<std::endl;
    }
    return 0;
}


std::string dogsAndCats(int n, int d, int c, int m, std::string s){
    long c_long=c;
    long catFoodNeeded=0;
    for (char animal:s) {
        if(animal=='D'){
            c_long-=catFoodNeeded;
            if(c_long<0) return "NO";
            catFoodNeeded=0;
            d--;
            c_long+=m;
        }else catFoodNeeded++;
    }
    if(d>=0 && c_long>=0) return "YES";
    else return "NO";
}

