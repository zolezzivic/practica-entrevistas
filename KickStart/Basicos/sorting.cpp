#include "sorting.hpp"

std::vector<int> mergeSort(std::vector<int>&v, int left, int right){
    if(right-left==0) return {};
    if(right-left==1) return {v[left]};
    int upper=right-1;
    int lower=left;
    int mid=(upper+lower)/2;
    while(upper>=lower){
        if(v[lower]>v[mid] && v[upper]<v[mid]){
            std::swap(v[upper],v[lower]);
            upper--;
            lower++;
        }else{
            if(v[lower]<=v[mid]) lower++;
            if(v[upper]>=v[mid]) upper--;
        }
        mid=(right+left)/2;
    }
    std::vector<int> vLower=mergeSort(v, left,mid);
    std::vector<int> vUpper=mergeSort(v, mid,right);

    int idxLower=0;
    int idxUpper=0;
    int idx=0;
    std::vector<int> res(right-left);
    while(idxLower<vLower.size() && idxUpper<vUpper.size()){
        res[idx]=std::min(vUpper[idxUpper], vLower[idxLower]);
        if(res[idx]==vUpper[idxUpper]) idxUpper++;
        else idxLower++;
        idx++;
    }
    while(idxUpper<vUpper.size()){
        res[idx]=vUpper[idxUpper];
        idxUpper++;
        idx++;
    }
    while(idxLower<vLower.size()){
        res[idx]=vLower[idxLower];
        idxLower++;
        idx++;
    }
    return res;
}

int getMaxIdx(std::vector<int>&v, int left, int right){
    int max=INT32_MIN;
    int idx=-1;
    for (int i = left; i < right; ++i) {
        max=std::max(max, v[i]);
        if(max==v[i]) idx=i;
    }
    return idx;
}

void quickSort(std::vector<int>&v, int left, int right){
    if(right-left<2) return;
    int random=std::rand()%(right-left);
    int pivot=left+random;
    int pivotValue=v[pivot];
    std::swap(v[left],v[pivot]);
    int lower=left+1;
    int upper=right-1;
    int idxMax= getMaxIdx(v, left+1, right);
    std::swap(v[upper], v[idxMax]);

    while(upper>=lower){
        if(v[lower]>pivotValue && v[upper]<pivotValue){
            std::swap(v[lower], v[upper]);
            lower++;
            upper--;
        }else{
            if(v[lower]<=pivotValue)lower++;
            if(v[upper]>=pivotValue)upper--;
        }
    }
    std::swap(v[left], v[upper]);
    quickSort(v,left, upper);
    quickSort(v, upper+1,right);
}


void siftDown(std::vector<int>&v, int i, int size){
    int left=2*i+1;
    int right=2*i+2;
    if(i<size/2){
        int maxChildIdx;
        if(left<size){
            if(right<size){
                maxChildIdx=std::max(v[left],v[right])==v[left] ? left:right;
            }else maxChildIdx=left;
        }else if (right<size) maxChildIdx=right;

        if(v[i]<v[maxChildIdx]){
            std::swap(v[i],v[maxChildIdx]);
            siftDown(v, maxChildIdx, size);
        }
    }
}


void heapify(std::vector<int>&v){
    for (int i = v.size()-1; i >=0 ; --i) {
        siftDown(v, i, v.size());
    }
}

int pop(std::vector<int>&v, int size){
    int res=v[0];
    v[0]=v[size];
    siftDown(v, 0, size);
    return res;
}

void heapSort(std::vector<int>&v){
    heapify(v);
    for (int i = v.size()-1; i >=0; --i) {
        int max=pop(v, i);
        v[i]=max;
    }
}



int binarySearch(std::vector<int>&v, int elem){
    int r=v.size();
    int l=0;
    int mid=(r+l)/2;
    while(r>l){
        mid=(r+l)/2;
        if(v[mid]==elem) return mid;
        else{
            if(v[mid]>elem){
                r=mid;
            }else{
                l=mid+1;
            }
        }
    }
    return (v[l]==elem?l:-1);
}








bool isSorted(std::vector<int>&v){
    if(v.size()<2) return true;
    for (int i = 0; i < v.size()-1; ++i) {
        if(v[i]>v[i+1]) return false;
    }
    return true;
}

bool test(){
    std::vector<int> v;
    std::vector<int> sorted;
    for (int i = 0; i < 100; ++i) {
        v=std::vector<int>(i);
        for (int j = 0; j < i; ++j) {
            v[j]=std::rand()% 300-150;
        }
        sorted=mergeSort(v,0,i);
        if(!isSorted(sorted)) return false;
        quickSort(v,0,i);
        if(!isSorted(v)) return false;
        for (int j = 0; j < i; ++j) {
            v[j]=std::rand()% 300-150;
        }
        heapSort(v);
        if(!isSorted(v)) return false;

    }
    return true;
}