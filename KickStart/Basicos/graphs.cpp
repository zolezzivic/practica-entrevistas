#include "graphs.hpp"
#include <unordered_set>
#include <queue>
#include <stack>


std::vector<int> bfs(std::vector<std::vector<int>>& ady, int node){
    std::vector<int> distances(ady.size(),INT32_MAX);
    distances[node]=0;
    std::unordered_set<int> visitedNodes;
    visitedNodes.insert(node);
    std::queue<int> nodes;
    nodes.push(node);
    while(!nodes.empty()){
        int currentNode=nodes.front();
        for (int i = 0; i < ady[currentNode].size(); ++i) {
            if(ady[currentNode][i]!=INT32_MAX && !visitedNodes.count(i)){
                nodes.push(i);
                visitedNodes.insert(i);
                distances[i]=distances[currentNode]+1;
            }
        }
        nodes.pop();
    }
    return distances;
}

std::vector<int> dfs(std::vector<std::vector<int>>& ady, int node){
    std::vector<int> pred(ady.size(),INT32_MAX);
    pred[node]=-1;
    std::unordered_set<int> visitedNodes;
    visitedNodes.insert(node);
    std::stack<int> nodes;
    nodes.push(node);
    while(!nodes.empty()){
        int currentNode=nodes.top();
        nodes.pop();
        for (int i = 0; i < ady[currentNode].size(); ++i) {
            if(ady[currentNode][i]!=INT32_MAX && !visitedNodes.count(i)){
                nodes.push(i);
                visitedNodes.insert(i);
                pred[i]=currentNode;
            }
        }
    }
    return pred;
}

//We could use a min heap to search for the closest node
int getClosestNode(std::vector<int>& distances, std::unordered_set<int>& visitedNodes){
    int min=INT32_MAX;
    int closestNode=-1;
    for (int i = 0; i < distances.size(); ++i) {
        if(!visitedNodes.count(i)){
            if(min>distances[i]){
                min=distances[i];
                closestNode=i;
            }
        }
    }
    return closestNode;
}


std::vector<int> dijkstra(std::vector<std::vector<int>>& ady, int node){
    int n=ady.size();
    std::unordered_set<int> visitedNodes;
    visitedNodes.insert(node);
    std::vector<int> distances(n, INT32_MAX);
    distances[node]=0;
    for(int i=0;i<n;i++){
        if(ady[node][i]!=INT32_MAX) distances[i]= ady[node][i];
    }
    for (int j = 0; j < n-1; ++j) {
        int closestNode= getClosestNode(distances, visitedNodes);
        visitedNodes.insert(closestNode);
        for (int i = 0; i < n; ++i) {
            if(ady[closestNode][i]!=INT32_MAX && !visitedNodes.count(i)){
                distances[i]=std::min(distances[i], distances[closestNode]+ady[closestNode][i]);
            }
        }
    }
    return distances;
}

std::vector<std::vector<int>> floyd(std::vector<std::vector<int>>& ady){
    std::vector<std::vector<int>> distances=ady;
    int n=ady.size();
    for (int k = 0; k < n; ++k) {
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                if(distances[i][k]!=INT32_MAX && distances[k][j]!=INT32_MAX){
                    distances[i][j]=std::min(distances[i][j], distances[i][k]+distances[k][j]);
                }
            }
        }
    }
    return distances;
}