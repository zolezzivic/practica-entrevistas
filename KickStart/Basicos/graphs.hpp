#ifndef BASICOS_GRAPHS_HPP
#define BASICOS_GRAPHS_HPP

#include <vector>
#include <cstdlib>
#include <iostream>



std::vector<int> bfs(std::vector<std::vector<int>>& ady, int node);
std::vector<int> dfs(std::vector<std::vector<int>>& ady, int node);
std::vector<int> dijkstra(std::vector<std::vector<int>>& ady, int node);
std::vector<std::vector<int>> floyd(std::vector<std::vector<int>>& ady);


#endif //BASICOS_GRAPHS_HPP
