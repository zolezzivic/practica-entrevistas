#ifndef BASICOS_SORTING_HPP
#define BASICOS_SORTING_HPP

#include <vector>
#include <cstdlib>
#include <iostream>

std::vector<int> mergeSort(std::vector<int>&v, int left, int right);
void quickSort(std::vector<int>&v, int left, int right);
void heapSort(std::vector<int>&v);
int binarySearch(std::vector<int>&v, int elem);
bool test();


#endif //BASICOS_SORTING_HPP
