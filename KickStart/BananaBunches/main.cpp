#include <iostream>
#include <vector>


std::vector<std::vector<std::vector<std::vector<std::vector<int>>>>> memo;
int bananaBunches(int k, std::vector<int>& trees);
int bananaBunchesBT(int k, std::vector<int> &trees, int idx, bool canLeave, bool prevInSection, int nSections);


int main() {
    int numberOfTest;
    scanf("%d", &numberOfTest);
    int N, K;
    for (int i = 0; i < numberOfTest; ++i) {
        scanf("%d", &N);
        scanf("%d", &K);
        std::vector<int> trees(N);
        int currTree;
        for (int j = 0; j < N; ++j) {
            scanf("%d", &currTree);
            trees[j]=currTree;
        }
        std::cout<<"Case #"<<i+1<<": "<<bananaBunches(K, trees)<<std::endl;
  }

    return 0;
}


int bananaBunchesBT(int k, std::vector<int> &trees, int idx, bool canLeave, bool prevInSection, int nSections) {
    if(k==0 && nSections<=2 && (trees.size()==idx || nSections<2)){
        return 0;
    }
    if(trees.size()==idx || k<0 ||nSections>2) return INT32_MAX;
    if(memo[idx][k][canLeave][prevInSection][nSections] == INT32_MAX){
        if(!canLeave){
            int buy= bananaBunchesBT(k - trees[idx], trees, idx + 1, false, false, nSections);
            if (buy<INT32_MAX) buy+=1;
            memo[idx][k][canLeave][prevInSection][nSections]= buy;
        }else{
            int buy,leave;
            buy= bananaBunchesBT(k - trees[idx], trees, idx + 1, canLeave, false, nSections);
            bool canLeaveNew=canLeave;
            int nSectionsNew=nSections;
            if(!prevInSection){
                canLeave= nSections != 1;
                nSectionsNew+=1;
            }
            leave= bananaBunchesBT(k, trees, idx + 1, canLeaveNew, true, nSectionsNew);
            if(buy<INT32_MAX){
                memo[idx][k][canLeave][prevInSection][nSections]= std::min(1 + buy, leave);
            }else memo[idx][k][canLeave][prevInSection][nSections]= leave;
        }
    }
    return memo[idx][k][canLeave][prevInSection][nSections];
}



int bananaBunches(int k, std::vector<int>& trees){
    int n=trees.size();
    memo=std::vector<std::vector<std::vector<std::vector<std::vector<int>>>>>(n+1, std::vector<std::vector<std::vector<std::vector<int>>>>(k+1, std::vector<std::vector<std::vector<int>>>(2,std::vector<std::vector<int>>(2, std::vector<int>(3,INT32_MAX)))));
    int res= bananaBunchesBT(k, trees, 0, true, true, 0);
    if(res==INT32_MAX) res=-1;
    return res;
}