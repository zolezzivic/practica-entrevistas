#include <iostream>
#include <vector>
#include <math.h>

std::vector<std::vector<int>>* magicMatrix(int n);
bool magicMatrixBT(int n, int row, int col, std::vector<std::vector<int>>& currentMatrix, std::vector<int>& usedNumbers);


int main() {
    magicMatrix(3);
    std::cout << "Hello, World!" << std::endl;
    return 0;
}


std::vector<std::vector<int>>* magicMatrix(int n){
    if (n==1){
        std::vector<std::vector<int>> res={{1}};
        return &res;
    }
    if (n==2) return nullptr;
    std::vector<std::vector<int>>* matrix = new std::vector<std::vector<int>>(n, std::vector<int>(n,0));
    std::vector<int> usedNum(std::pow(n,2), 0);
    if(magicMatrixBT(n,0,0, *matrix, usedNum)){
        for(int i=0; i<matrix->size(); i++){
            for (int j = 0; j < matrix->size(); ++j) {
                std::cout<<(*matrix)[i][j]<<" ";
            }
            std::cout<<std::endl;
        }
        return matrix;
    }

    std::cout<<"No lo encontro"<<std::endl;
    return nullptr;
}

bool checkSums(int row, int col,std::vector<std::vector<int>>& matrix){
    if(row==0) return true;
    int sum=0;
    int sumRow=0;
    int sumCol=0;
    int sumDiagDer=0;
    int sumDiagIzq=0;
    for (int i = 0; i < matrix.size(); ++i) {
        sum+= matrix[0][i];
    }
    if(std::pow(matrix.size(),2)>=sum) return false;                 //I wont be able to get this sum if only one number is bigger than it

    for (int k = 1; k < row; ++k) {
        for (int i = 0; i < matrix.size(); ++i) {
            sumRow+=matrix[k][i];
        }
        if(sumRow!=sum) return false;
        sumRow=0;
    }
    if(row==matrix.size()-1 && col>0){
        for (int i = 0; i < matrix.size(); ++i) {
            sumDiagDer+=matrix[i][matrix.size()-1-i];
        }
        if(sumDiagDer!=sum) return false;
        for (int j = 0; j < col; ++j) {
            for (int i = 0; i < matrix.size(); ++i){
                sumCol+=matrix[i][j];
            }
            if(sumCol!=sum) return false;
            sumCol=0;
        }
    }

    if(row==matrix.size() && col==0){
        for (int i = 0; i < matrix.size(); ++i) {
            sumDiagIzq+=matrix[i][i];
        }
        if(sumDiagIzq!=sum) return false;
    }
    return true;
}

bool magicMatrixBT(int n, int row, int col, std::vector<std::vector<int>>& currentMatrix, std::vector<int>& usedNumbers){
    if(!checkSums(row, col,currentMatrix)) return false;
    if(row==n && col==0) return true;
    for (int i = 1; i <= std::pow(n,2); ++i) {
        if(usedNumbers[i-1]==0){
            currentMatrix[row][col]=i;
            usedNumbers[i-1]=1;
            int newCol= col+1<n? col+1:0;
            int newRow= newCol==0? row+1:row;
            if(magicMatrixBT(n, newRow, newCol, currentMatrix, usedNumbers)) return true;
            currentMatrix[row][col]=0;
            usedNumbers[i-1]=0;
        }
    }
    return false;
}
