#include <iostream>
#include <vector>

typedef std::vector<std::vector<std::vector<int>>> memo ;

class Solution {
public:
    Solution(): m(){}
    int totalFruit(std::vector<int>& fruits);
    int fruitsAux(std::vector<int> &fruits, int basket1, int basket2, int startTree);

private:
    memo m;
};




int main() {
    Solution S=Solution();
    std::vector<int> f={3,3,3,1,2,1,1,2,3,3,4};
    int res=S.totalFruit(f);
    std::cout << res << std::endl;
    return 0;
}



int Solution::totalFruit(std::vector<int>& fruits){
    int b1=-1;
    int b2=-1;
    int b1LastConsecutivePos=-1;
    int b2LastConsecutivePos=-1;
    int maximo=0;

    int current=0;
    for(int i=0; i<fruits.size();i++){
        if(b1==fruits[i]){
            current++;
            if(fruits[i-1]!=b1) b1LastConsecutivePos=i; //If the previous fruit is different from the current one, I keep my position as the last consecutive one
        }else if( b2==fruits[i]){
            current++;
            if(fruits[i-1]!=b2) b2LastConsecutivePos=i;
        }
        else if(b1==-1) {
            b1=fruits[i];
            b1LastConsecutivePos=i;
            current++;
        }
        else if(b2==-1) {
            b2=fruits[i];
            b2LastConsecutivePos=i;
            current++;
        }
        else{
            maximo=std::max(maximo, current);
            if(fruits[i-1]==b1) {
                b2=fruits[i];
                b2LastConsecutivePos=i;
                current=i-b1LastConsecutivePos+1;
            }
            else if (b2==fruits[i-1]){
                b1LastConsecutivePos=i;
                b1=fruits[i];
                current= i - b2LastConsecutivePos + 1;
            }else{
                if(b1LastConsecutivePos <= b2LastConsecutivePos) {  //I will replace the basket with the oldest fruit
                    b1=-1;
                    b1LastConsecutivePos=-1;
                    b2=fruits[i];
                    b2LastConsecutivePos=i;
                }else{
                    b2=-1;
                    b2LastConsecutivePos=-1;
                    b1=fruits[i];
                    b1LastConsecutivePos=i;
                }
                current=1;
            }
        }
    }
    return std::max(current, maximo);
}



/*
int Solution::totalFruit(std::vector<int>& fruits) {
    m=memo(fruits.size()+1, std::vector<std::vector<int>>(fruits.size()+1, std::vector<int> (fruits.size(), -1)));
    return fruitsAux(fruits, -1, -1, 0);
}

int Solution::fruitsAux(std::vector<int> &fruits, int basket1, int basket2, int startTree){
    if(startTree>=fruits.size()) return 0;
    if(m[basket1+1][basket2+1][startTree]==-1){
        if(basket1==-1 && basket2==-1){
            int addToB1=1+fruitsAux(fruits, fruits[startTree], basket2, startTree+1);
            int addtoB2=1+fruitsAux(fruits, basket1, fruits[startTree], startTree+1);
            int ignore=fruitsAux(fruits, basket1, basket2, startTree+1);
            m[basket1+1][basket2+1][startTree]=std::max(std::max(addToB1,addtoB2),ignore);
        }
        else if(fruits[startTree]==basket2 || fruits[startTree]==basket1){
            m[basket1+1][basket2+1][startTree]= 1+fruitsAux(fruits, basket1, basket2, startTree+1);
        }
        else if(basket1==-1){
            int addToB1=1+fruitsAux(fruits, fruits[startTree], basket2, startTree+1);
            //int ignore=fruitsAux(fruits, basket1, -1, startTree+1);
            m[basket1+1][basket2+1][startTree]= addToB1;
        }
        else if(basket2==-1){
            int addToB2=1+fruitsAux(fruits, basket1, fruits[startTree], startTree+1);
            //int ignore=fruitsAux(fruits, -1, basket2, startTree+1);
            m[basket1+1][basket2+1][startTree]=addToB2;
        }else{
            m[basket1+1][basket2+1][startTree]=0;
        }
    }
    return m[basket1+1][basket2+1][startTree];
}
*/