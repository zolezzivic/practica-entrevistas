#include <iostream>
#include <unordered_map>
#include <list>

class TreeNode{
public:
    int val;
    TreeNode* left;
    TreeNode* right;
    TreeNode(int v):val(v){};
    TreeNode(int v, TreeNode* l, TreeNode* r):val(v), left(l), right(r){};

};

void getHorizontalDistance(TreeNode* root, int horizontalDistance, std::unordered_map<int, std::list<int>>& dicOfDistances){
    if(root== nullptr) return;
    dicOfDistances[horizontalDistance].push_back(root->val);
    getHorizontalDistance(root->left, horizontalDistance-1, dicOfDistances);
    getHorizontalDistance(root->right, horizontalDistance+1, dicOfDistances);
}


void printVerticalOrder(TreeNode* root){
    std::unordered_map<int, std::list<int>> horizontalDistances;
    getHorizontalDistance(root, 0, horizontalDistances);
    std::list<int> keys;
    int maxKey=INT32_MIN;
    int minKey=INT32_MAX;
    for(auto distance: horizontalDistances){
        keys.push_back(std::get<0>(distance));
        if(maxKey<std::get<0>(distance)) maxKey=std::get<0>(distance);
        if(minKey>std::get<0>(distance)) minKey=std::get<0>(distance);

    }

    for (int i = minKey; i <= maxKey; ++i) {
        if(horizontalDistances.count(i)){
            for(int node: horizontalDistances[i]) printf("%i ", node);
            printf("\n");
        }

    }
}

int main() {
    TreeNode* tree= new TreeNode(1, new TreeNode(2, new TreeNode(4), new TreeNode(5)), new TreeNode(3, new TreeNode(6), new TreeNode(7, new TreeNode(8), new TreeNode(9))));
    printVerticalOrder(tree);

    return 0;
}
