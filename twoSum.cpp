#include <iostream>
#include <vector>
#include <unordered_map>

std::vector<int> twoSum(std::vector<int>& nums, int target){
    std::unordered_map<int, int> apariciones;
    for (int i = 0; i < nums.size(); ++i) {
        apariciones.insert({nums[i], i});
    }
    for (int j = 0; j < nums.size(); ++j) {
        int complemento=target-nums[j];
        if(apariciones.count(complemento)){
            int idxComplemento=apariciones[complemento];
            if(idxComplemento!=j) return {j, apariciones[complemento]};
        }
    }
}

int main() {
    std::vector<int> v={3,2,4};
    int target=6;
    std::vector<int> res=twoSum(v, target);
    printf("%i y %i\n", res[0], res[1]);

    return 0;
}