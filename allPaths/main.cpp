#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;



vector<vector<int>> allPathsSourceTargetAux(vector<vector<int>>& graph, int startNode){
    if(startNode==graph.size()-1) return {{startNode}};
    if(graph[startNode].empty()) return{};

    vector<vector<int>> res;

    vector<vector<int>> currentPaths;
    for(int neig: graph[startNode]){
        currentPaths=allPathsSourceTargetAux(graph, neig);

        for(vector<int>& path: currentPaths){
            path.push_back(startNode);
            res.push_back(path);
        }
    }
    return res;
}

vector<vector<int>> allPathsSourceTarget(vector<vector<int>>& graph) {
    vector<vector<int>> paths=allPathsSourceTargetAux(graph, 0);
    for(vector<int>& path: paths) {
        reverse(path.begin(), path.end());
    }

    return paths;
}



int main() {

    vector<vector<int>> graph={{1,2},{3},{3},{}};

    vector<vector<int>> paths= allPathsSourceTarget(graph);

    std::cout << "Hello, World!" << std::endl;
    return 0;
}
