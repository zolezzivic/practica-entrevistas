#include <iostream>
#include<vector>
#include<unordered_set>
#include <unordered_map>
#include <queue>
using namespace std;

int maxIdx(std::vector<vector<int>> & v, int left, int right){
    int res=INT32_MIN;
    int idx=-1;
    for (int i=left; i<right; i++) {
        if (v[i][2]>res) res=v[i][2], idx=i;
    }
    return idx;
}

void quickSort(std::vector<vector<int>> &v, int left, int right){
    if (right-left<=1) return;
    //We choose a random item of the vector as pivot.
    int pivotIdx = left+ rand() % (right-left);
    int pivot = v[pivotIdx][2];
    int upper=right-1;
    int lower =left+1;
    //We put the pivot element at the first position of the array
    std::swap(v[left], v[pivotIdx]);
    int maxIndex = maxIdx(v, left+1, right);
    //We put the max element (here we dont consider the pivot element) at the end of the vector. This will ensure lower wont be bigger than size-1
    std::swap(v[right-1], v[maxIndex]);
    while (upper >= lower){
        if (v[lower][2]<=pivot) lower++;
        else if (v[upper][2]>=pivot) upper--;
        else if (v[lower][2]>pivot && v[upper][2]<pivot) {
            std::swap(v[lower],v[upper]);
            lower++;
            upper--;
        }
    }
    //We put the pivot in its place (we swap it with a smaller element)
    std::swap(v[left], v[upper]);
    //We sort the elements smaller than the pivot and the bigger ones.
    quickSort(v, left, upper);
    quickSort(v, upper+1, right);

}

int findTimeFirstMeetingOfZeroAndFirst(vector<vector<int>>& meetings, int firstPerson){
    int res=INT32_MAX;
    for(vector<int> meeting: meetings){
        if((meeting[0]==0 && meeting[1]==firstPerson) || meeting[1]==0 && meeting[0]==firstPerson) res=min(res, meeting[2]);
    }
    return res;
}

vector<int> findAllPeople(int n, vector<vector<int>>& meetings, int firstPerson) {
    int t0=findTimeFirstMeetingOfZeroAndFirst(meetings, firstPerson);
    unordered_set<int> knowSecret;
    knowSecret.insert(0);
    knowSecret.insert(firstPerson);
    unordered_map<int, vector<int>> peopleMeetings;

    for(vector<int> meeting: meetings){
        if(meeting[2]>=t0){
            peopleMeetings[meeting[0]].push_back(meeting[1]);
            peopleMeetings[meeting[1]].push_back(meeting[0]);
        }
    }

    queue<int> peopleWhoKnow;
    peopleWhoKnow.push(0);
    peopleWhoKnow.push(firstPerson);
    int currentPerson;

    while(!peopleWhoKnow.empty()){
        currentPerson=peopleWhoKnow.front();
        for(int person: peopleMeetings[currentPerson]){
            if(! knowSecret.count(person)){
                knowSecret.insert(person);
                peopleWhoKnow.push(person);
            }
        }
        peopleWhoKnow.pop();
    }
    vector<int> res;
    for(int person: knowSecret) res.push_back(person);
    return res;

}




int main() {
    vector<vector<int>> v ={{1,2,5},{2,3,8}, {1,5,10}};
    vector<int> res=findAllPeople(6,v,1);
    std::cout << "Hello, World!" << std::endl;
    return 0;
}
