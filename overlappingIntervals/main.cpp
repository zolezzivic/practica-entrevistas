#include <iostream>
#include <string>
#include <vector>
#include <queue>
#include <stack>
#include <math.h>
#include <unordered_set>

using namespace std;

struct distanceAndPoint{
    float distance;
    int pointIdx;
    distanceAndPoint(): distance(-1), pointIdx(-1){}
    distanceAndPoint(float d, int p): distance(d), pointIdx(p) {}
    bool operator < (const struct distanceAndPoint& other) const{
        return (distance<other.distance);
    }
};

template<class T> void printVector(vector<T>& v){
    for (T elem:v) cout<<elem<<",";
    printf("\n");
}


struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

struct coordinate{
    int x;
    int y;
    coordinate(): x(0), y(0){};
    coordinate(int x, int y): x(x), y(y){};
    bool operator < (const struct coordinate other) const {
        if(x<other.x) return true;
        if(x==other.x) return y<other.y;
        return false;
    }
    bool operator == (const struct coordinate other) const{
        if(x!=other.x) return false;
        return y == other.y;
    }



};


//------------------------------------------------------------------------------


vector<vector<int>> mergeIntervals(vector<vector<int>>& intervals, int low, int high){
    if(high<=low) return {};
    if(high==low+1) return {intervals[low]};
    int mid=(high+low)/2;

    vector<vector<int>> leftIntervals=mergeIntervals(intervals, low, mid);
    vector<vector<int>> rightIntervals=mergeIntervals(intervals, mid, high);

    vector<vector<int>> res(high-low);
    int idxLeft=0;
    int idxRight=0;
    int idxRes=0;
    while(idxLeft<(mid-low) && idxRight<(high-mid)){
        if(leftIntervals[idxLeft][0]<rightIntervals[idxRight][0] || (leftIntervals[idxLeft][0]==rightIntervals[idxRight][0] && leftIntervals[idxLeft][1]<rightIntervals[idxRight][1])){
            res[idxRes]=leftIntervals[idxLeft];
            idxRes++;
            idxLeft++;
        }else {
            res[idxRes]=rightIntervals[idxRight];
            idxRes++;
            idxRight++;
        }
    }
    while(idxLeft<mid-low){
        res[idxRes]=leftIntervals[idxLeft];
        idxRes++;
        idxLeft++;
    }
    while(idxRight<high-mid){
        res[idxRes]=rightIntervals[idxRight];
        idxRes++;
        idxRight++;
    }

    return res;
}



int eraseOverlapIntervals(vector<vector<int>>& intervals) {
    if(intervals.size()<=1) return 0;
    vector<vector<int>> intervalsSorted=mergeIntervals(intervals,0,intervals.size());
    vector<int> currentInterval(2);
    vector<int> nextInterval(2);
    int currentTime=intervals[0][0];
    int res=0;
    for (int i = 0; i < intervals.size()-1; ++i) {
        currentInterval = intervalsSorted[i];
        nextInterval = intervalsSorted[i+1];
        if(currentInterval[1] > nextInterval[0]){   //saco uno
            res++;
            currentTime=currentInterval[1];
            if(currentInterval[1]<=nextInterval[1]) {
                i++;
                if(i+1==intervals.size()-1){
                    if(intervalsSorted[i+1][0]<currentTime) res++;
                }
            }
            else currentTime=nextInterval[1];
        }else currentTime=currentInterval[1];
    }

    return res;
}


int main() {
    vector<vector<int>> intervals={{1,2},{1,2},{1,2}};
    //vector<vector<int>> intervals={{1,4},{1,3},{1,2}};
    int res= eraseOverlapIntervals(intervals);
    printf("%i\n", res);
    return 0;
}
