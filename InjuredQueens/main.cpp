#include <iostream>
#include <vector>

int waysOfArrangingQueens(int n);
int waysOfArrangingQueensBT(int i, int n, std::vector<int>& queens);
bool test(std::vector<int>&queens);


int main() {
    std::cout << waysOfArrangingQueens(6) << std::endl;
    return 0;
}

int waysOfArrangingQueens(int n){
    std::vector<int> queens(n, -1);
    return waysOfArrangingQueensBT(0,n, queens);
}

std::vector<int> possibleRows(int col, int n, std::vector<int>&queens){
    std::vector<int> res;
    for (int row = 0; row < n; ++row) {
        if(col+1<n){
            if (row+1<n && queens[col+1]==row+1) continue;
            if(row-1>=0 && queens[col+1]==row-1) continue;
            if(queens[col+1]==row) continue;
        }
        if(col-1>=0){
            if (row+1<n && queens[col-1]==row+1) continue;
            if(row-1>=0 && queens[col-1]==row-1) continue;
            if(queens[col-1]==row) continue;
        }
        res.push_back(row);
    }
    return res;
}

int waysOfArrangingQueensBT(int i, int n, std::vector<int>& queens){
    if(i>=n && test(queens)) {
        return 1;
    }

    std::vector<int> possiblePos= possibleRows(i, n,queens);
    int sum=0;
    for(int row:possiblePos){
        queens[i]=row;
        sum+=waysOfArrangingQueensBT(i+1,n,queens);
        queens[i]=-1;
    }
    return sum;
}


bool test(std::vector<int>&queens){
    int n=queens.size();
    for(int col=0; col<queens.size();col++){
        int row=queens[col];
        if(col<n){
            if (row+1<n && (queens[col+1]==row+1)) return false;
            if(row-1>=0 && queens[col+1]==row-1) return false;
            if(queens[col+1]==row) return false;
        }
        if(col>0){
            if (row+1<n && queens[col-1]==row+1 ||queens[col-1]==row) return false;
            if(row-1>=0 && queens[col-1]==row-1) return false;
            if(queens[col-1]==row) return false;
        }
    }
    return true;
}